# Typy danych

## Typy skalarne

### Typ logiczny (_Boolean_)

Zmienna typu _boolean_ przyjmuje jedną z dwóch stałych wartości (niewrażliwą na wielkość znaków): __TRUE__ lub __FALSE__

Typowe zastosowanie:

```

#!php
<?php
$foo = true;
if ($foo) {
    echo 'bar';
}

```

Sprawdźmy wynik rzutowania innych typów na typ logiczny:

```

#!php
<?php
var_dump((bool)false);
var_dump((bool)'false');
var_dump((bool)0);
var_dump((bool)0.0);
var_dump((bool)'');
var_dump((bool)'0');
var_dump((bool)array());
var_dump((bool)null);
var_dump((bool)22);
var_dump((bool)-1);
var_dump((bool)'foo');
var_dump((bool)2.3e5);
var_dump((bool)array('1', '2'));

```

### Liczba całkowita (_Integer_)

```
#!php

<?php
$a = 123;
var_dump($a);
$b = '123';
var_dump($b);
var_dump((int)$b);

```

Rozmiar:

```

#!php
<?php
echo PHP_INT_SIZE;
echo "\r\n";
echo PHP_INT_MAX;
echo "\r\n";

```

```

#!php
<?php
$maxInt = PHP_INT_MAX;
var_dump($maxInt + 1);

```

### Liczby rzeczywiste (_Float_ , _Double_, _Real numbers_)

```

#!php

<?php
$a = 1.234; 
$b = 1.2e3; 
$c = 7E-10;

```

### Ciągi znaków (_String_)

```

#!php
<?php
var_dump('foo');
var_dump('bar');

```

## Typy złożone

### Tablice  (_Array_)

#### Tablice indeksowane numerycznie

```

#!php
<?php
$tab = array('a', 'b', 'c', 'd');
var_dump($tab);
$tab = array(0 => 'a', 1 => 'b', 2 => 'c', 3 => 'd');
var_dump($tab);
$tab = array();
$tab[] = 'a';
$tab[] = 'b';
$tab[] = 'c';
$tab[] = 'd';
$tab = array();
$tab[77] = 'e';
$tab = array();

```

#### Tablice asocjacyjne

```

#!php

<?php
$tab = array(
    'firstname' => 'John', 
    'surname' => 'Doe'
);
var_dump($tab);
$persons = array(
    array('firstname' => 'Ann', 'surname' => 'Smith'),
    array('firstname' => 'John', 'surname' => 'Doe')
);
var_dump($persons);

```

## Typy specjalne

### Zasoby (_Resources_)

Zasobami będzie na przykład uchwyt do pliku czy połączenie z bazą danych.

### _NULL_

Wartość specjalna _NULL_ reprezentuje zmienną bez wartości. 

```

#!php
<?php
$foo = NULL;

if (is_null($foo)) {
    echo 'bar';
}


```

## Użyteczne funkcje

```

#!php
<?php
var_dump('foo');
echo gettype(12);
echo "\r\n";

```
