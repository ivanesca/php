# Silex V - wygląd aplikacji 

W jaki sposób nadać wygląd aplikacji zobaczymy na przykładzie biblioteki [Bootstrap](http://getbootstrap.com "Bootstrap").

W katalogu `web` aplikacji utwórzmy podkatalogi `assets/bootstrap`:

```
#!bash
cd ~/silex
mkdir -p web/assets/bootstrap
```

Następnie z [oficjalnej strony](http://getbootstrap.com/getting-started/#download) pobieramy paczkę z bibliteką i rozpakowujemy ją do katalogu `web/assets/bootstrap`. Po tej operacji w nim powinny pojawić się katalogi `css`, `fonts`, `js`. Pozostaje zmodyfikować szablon `base.twig`:

```
#!twig
<html>
    <head>
        {% block head %}
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link href="{{ app.request.basepath }}/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
            <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
            <script src="{{ app.request.basepath }}/assets/bootstrap/js/bootstrap.min.js"></script>
            <title>{% block title %}{% endblock %}</title>
        {% endblock %}
    </head>
    <body>
        <div id="content">{% block content %}{% endblock %}</div>
    </body>
</html>
```

Więcej na temat załączania plików wewnątrz szablonów można znaleźć w [dokumentacji](http://silex.sensiolabs.org/doc/cookbook/assets.html "Managing Assets in Templates").






