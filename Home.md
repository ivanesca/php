# Spis treści

Poniżej znajdują się odnośniki do zagadnień omawianych podczas zajęć:

* [[Wprowadzenie do PHP]]
* [[Typy danych]]
* [[Zmienne]]
* [Stałe](Stale)
* [[Operatory]]
* [Strunktury kontrolne](Struktury_kontrolne)
* [[Funkcje]]
* [[Operacje na tablicach]]
* [Oddzielanie logiki aplikacji od wyglądu](oddzielanie_logiki_aplikacji_od_wygladu/Home)
* [[Klasy i obiekty]]
* [Silex I - Wprowadzenie](Silex_i_-_wprowadzenie)
* [Silex II - szablony Twig](Silex_ii_-_szablony_Twig)
* [Silex III - kontrolery akcji](Silex_iii_-_kontrolery_akcji)
* [Silex IV - tłumaczenia](Silex_iv_-_tlumaczenia)
* [Silex V - wygląd aplikacji](Silex_v_-_wyglad_aplikacji)
* [Silex VI - wprowadzenie do obsługi bazy danych](Silex_vi_-_wprowadzenie_do_obslugi_bazy_danych)
* [Silex VII - mechanizm CRUD - przykład](Silex_vii_-_mechanizm_CRUD_-_przyklad)
* [Silex VIII - mechanizm CRUD - uzupełnienie](Silex_viii_-_mechanizm_CRUD_-_uzupelnienie)
* [Silex IX - mechanizm kontroli dostępu](Silex_ix_-_mechanizm_kontroli_dostepu)
* [[PHP_CodeSniffer]]
* [Generowanie dokumentacji na podstawie komentarzy phpDoc](Generowanie_dokumentacji_na_podstawie_komentarzy_phpDoc)