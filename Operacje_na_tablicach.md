# Operacje na tablicach

## Przygotowanie

Aby nie powielać za każdym razem funkcji `ini_set()` odpowiedzialnych za raportowanie błędów utwórzmy sobie skrypt `dev.php` o zawartości:

```

#!php
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', TRUE);

```

a następnie załączmy go w każdym kolejnym skrypcie, który będziemy pisać następująco:

```

#!php
<?php
require dirname(__FILE__) . '/dev.php';

// ... 

```

## Pobieranie danych z żądania POST lub GET

Mamy przykładowe wywołanie skryptu `http://example.com/script.php?var1=value1&var2=val2` Aby móc skorzystać z danych przesłanych w żądaniu możemy pobrać je tak:

```

#!php
<?php
require dirname(__FILE__) . '/dev.php';

$var1 = isset($_GET['var1'])?$_GET['var1']:'';
// ...

```

Przy takim rozwiązaniu zmienne, które nas interesują zawsze istnieją, przy czym jeżeli użytkownik ich nie uzupełnił mają co najmniej wartość pustą. Skoro wiemy jakich zmiennych oczekujemy w żądaniu, proces ich pobierania z żądania możemy zautomatyzować wprowadzając tablicę zawierającą listę pól jakie powinny znaleźć się w żądaniu:

```

#!php
<?php
require dirname(__FILE__) . '/dev.php';

$request_pattern = array('var1', 'var2');

$data = array();
foreach ($request_pattern as $field) {
    $data[$field] = isset($_GET[$field])?$_GET[$field]:'';
}

```

### Ćwiczenie I

W formularzu przesyłanym metodą POST mamy dane pola: `firstname`, `lastname`, `email`, `age`. W sposob analogiczny do powyższedgo odebrać wszystkie dane z formularza, oraz przed przepisaniem ich do pomocniczej tablicy wywołać funkcję usuwającą zbędne białe znaki z początku i końca ciągu (`trim()`).

### Ćwiczenie II

Napisać funkcję `get_request_data()`, która przyjmie dwa argumenty: tablicę, z której pobieramy dane (POST/GET) i tablicę wzorca, a na wyjściu zwraca tablicę z danymi pobranymi z żądania, przy czym jeżeli użytkownik ich nie uzupełnił, ustawia wartość pola na ciąg pusty.
 
## Sortowanie tablic
 
### Ćwiczenie I

Tablicę: `$tab = array('plum', 'orange', 'banana', 'apple');` przesortuj w porządku leksykalnym.

### Ćwiczenie II 

Tablicę: `$tab = array('name' => 'Mark', 'surname' => 'Brown', 'age' => '21');` przesortuj w porządku leksykalnym.

### Ćwiczenie III

Tablicę:

```
#!php
<?php
require dirname(__FILE__) . '/dev.php';

$persons = array(
    array('name' => 'Mark', 'surname' => 'Brown'),
    array('name' => 'Ann', 'surname' => 'Smith'),
    array('name' => 'John', 'surname' => 'Doe')
);
```

posortuj według imion, a następnie względem nazwisk.

## Filtrowanie elementów tablicy

```
#!php
<?php
require dirname(__FILE__) . '/dev.php';

$data = range(1,10);

function is_odd($value) {
    return ($value % 2);
}

var_dump(array_filter($data, 'is_odd'));
sort($data);
var_dump($data);
```

## Modyfikacja wszystkich elementów tablicy

```

#!php
<?php
require dirname(__FILE__) . '/dev.php';

$data = range(1,10);

function power($value)
{
    return (int)($value * $value);
}

var_dump($data);

$data = array_map('power', $data);

var_dump($data);

```

### Ćwiczenie I

Dla danej tablicy `$tab = array('plum', 'orange', 'banana', 'apple');` zamienić wszystkie litery na wielkie używając `array_map()`.

## Inne przydatne funkcje:

 * [implode()](http://php.net/manual/en/function.implode.php)
 * [explode()](http://php.net/manual/en/function.explode.php)
 * [is_array()](http://php.net/manual/en/function.is-array.php)
 * [in_array()](http://php.net/manual/en/function.in-array.php)
 * [array_key_exists()](http://php.net/manual/en/function.array-key-exists.php)
 * [array_keys()](http://php.net/manual/en/function.array-keys.php)
 * [array_merge()](http://php.net/manual/en/function.array-merge.php)
 
















