# Generowanie dokumentacji na podstawie komentarzy PhpDoc

Generowanie dokumentacji na podstawie komentarzy `phpDoc` należy wykonać lokalnie ponieważ nie ma możliwości zmiany uprawnień dla plików tymczasowych tworzonych na wierzbie.

`phpDocumentator`'a instalujemy poleceniem:

```
#!bash
cd ~/silex_test
curl -OL http://www.phpdoc.org/phpDocumentor.phar
```

Aby utworzyć dokumentację należy utworzyć katalog, w którym ma zostać zapisana:

```
#!bash
cd ~/silex_test
mdkir -p docs
```

a następnie wywołać polecenie

```
#!bash
php phpDocumentor.phar -d src/ -t docs/
```

W wyniku generowania dokumentacji, na ekranie pojawia się raport, który może, poza informacjami na temat wykonania poszczególnych kroków zawierać również informacje o błędach do skorygowania, np:

```
#!bash
(...)
Parsing files
Parsing silex_test/src/Form/AlbumForm.php
  Only one @package tag is allowed
Parsing silex_test/src/Controller/AlbumsController.php
Parsing silex_test/src/Controller/HelloController.php
  No summary for class \Controller\HelloController
Parsing silex_test/src/Model/AlbumsModel.php
  No summary for class \Model\AlbumsModel
(...)
```

Pełna dokumentacja `phpDocumentator`'a znajduje się pod adresem http://www.phpdoc.org/.

