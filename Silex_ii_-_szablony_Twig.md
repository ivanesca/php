# Silex II - szablony Twig

Oficjalna dokumentacja systemu szablonów Twig znajduje się pod adresem http://twig.sensiolabs.org/.

## Przygotowanie

```
#!bash
vim ~/silex_test/web/index.php
```

```
#!php
<?php
// ...
$app->get('/data', function () use ($app, $data) {
    return $app['twig']->render(
        'data.twig', array('data' => $data)
    );
});
// ...
```

## Pętle

```
#!bash
vim ~/silex_test/src/views/data.twig

```

```
#!twig
<h1>Data</h1>
<ul>
    {% for row in data %}
        <li>{{ row.name }}, {{ row.email }}</li>
    {% endfor %}
</ul>
```

## Filtry

```
#!bash
vim ~/silex_test/src/views/data.twig
```

```
#!twig
<h1>Data</h1>
<ul>
    {% for row in data %}
        <li>{{ row.name|e }}, {{ row.email|e }}</li>
    {% endfor %}
</ul>
```

```
#!bash
vim ~/silex_test/src/views/data.twig
```

```
#!twig
<h1>Data</h1>
<ul>
    {% for row in data %}
        <li>{{ row.name|upper }}, {{ row.email|e }}</li>
    {% endfor %}
</ul>
```

## Komentarze

```
#!twig
{# loop data array #}
<h1>Data</h1>
<ul>
    {% for row in data %}
        <li>{{ row.name|upper }}, {{ row.email|e }}</li>
    {% endfor %}
</ul>
```

## Instrukcje warunkowe

```
#!bash
vim ~/silex_test/web/index.php
```

```
#!php
<?php
// ...
$app->get('/data/{id}', function (Silex\Application $app, $id) use ($data) {
    $item = isset($data[$id])?$data[$id]:array();
    return $app['twig']->render(
        'data_item.twig', array('item' => $item)
    );
});
// ...
```

```
#!bash
vim ~/silex_test/src/views/data_item.twig
```

```
#!twig
<h1>Item</h1>
<p>
    {% if item|length > 0 %}
        {{ item.name|upper }}, {{ item.email|e }}
    {% else %}
        Invalid data.
    {% endif %}
</p>
```

## Dziedziczenie szablonów

```
#!bash
vim ~/silex_test/src/views/base.twig
```

```
#!twig
<!DOCTYPE html>
<html>
    <head>
        {% block head %}
            <title>{% block title %}{% endblock %}</title>
        {% endblock %}
    </head>
    <body>
        <div id="content">{% block content %}{% endblock %}</div>
    </body>
</html>
```

```
#!bash
vim ~/silex_test/src/views/hello.twig
```

```
{% extends 'base.twig' %}

{% block title %}Hello World{% endblock %}

{% block head %}
    {{ parent() }}
    <style type="text/css">
        h1 { color: red; }
    </style>
{% endblock %}
{% block content %}
    <h1>
        Hello {{ name }}!
    </h1>
    <p>
        Some static text.
    </p>
{% endblock %}
```

## Załączanie szablonów

```
#!twig
{% include 'sidebar.html' %}
```

```
#!twig
{% for item in data %}
    {% include 'data_item.html' %}
{% endfor %}
```

## Ćwiczenie I

Przygotować mechanizm wyświetlania listy wszystkich kursów walut [z danych](oddzielanie_logiki_aplikacji_od_wygladu/Dane_zrodlowe)

## Ćwiczenie II

Przygotować mechanizm wyświetlania pojedycznego krusu waluty na podstawie wprowadzonego w adresie jej kodu ISO.
