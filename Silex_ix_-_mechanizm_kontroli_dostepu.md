# Silex IX - mechanizm kontroli dostępu

Mechanizm autentykacji i autoryzacji użytkownika oprzemy o [SecurityServiceProvider](http://silex.sensiolabs.org/doc/providers/security.html "SecurityServiceProvider").

## Struktura bazy danych

Na poniższym _listingu_ przedstawiam strukturę tabel do załadowania do bazy danych:

```
#!sql
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_USER');

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` char(32) COLLATE utf8_bin NOT NULL,
  `password` char(128) COLLATE utf8_bin NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_users_1` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `users`
  ADD CONSTRAINT `FK_users_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
```

## Instalacja i wstępna konfiguracja SecurityServiceProvider

Zaczynamy od dodania nowego modułu `symfony/security` w pliku `composer.json`:

```
#!json
{
    "require": {
        "doctrine/dbal": "2.2.*",
        "silex/silex": "~1.2",
        "symfony/config": "~2.3",
        "symfony/form": "~2.3",
        "symfony/locale": "~2.3",
        "symfony/security": "~2.3",
        "symfony/security-csrf": "~2.3",
        "symfony/translation": "~2.3",
        "symfony/twig-bridge": "~2.3",
        "symfony/validator": "~2.3",
        "symfony/yaml": "~2.3"
    },
    "autoload": {
        "psr-0": {
            "Controller" : "src/",
            "Model" : "src/",
            "Form" : "src/"
        }
    }
}
```

Jak zwykle, aktualizujemy biblioteki poleceniem:

```
#!bash
php composer.phar update
```

a następnie rejestrujemy moduł w pliku `index.php` wraz z roboczą regułą dla _firewalla_:

```
#!php
<?php
// ...
$app->register(
    new Silex\Provider\SecurityServiceProvider(), array(
        'security.firewalls' => array(
            'unsecured' => array(
                'anonymous' => true,
            ),
        ),
    )
);
// ...
```

Uruchamiamy aplikację i sprawdzamy czy nadal działa poprawnie.

## Testowi użytkownicy

Na potrzeby testów utworzymy dwóch użytkowników: administratora i zwykłego użytkownika. Hasła dla nich, możemy wygenerować dodając roboczo w kodzie (akcja kontrolera lub plik `index.php`) linię:

```
#!php
<?php
// ... 
var_dump($app['security.encoder.digest']->encodePassword('PASSWORD_TO_ENCODE', ''));
// ...
```

Przykładowe polecenia wstawiajace użytkowników do bazy danych przedstawiam poniżej:

```
#!sql
-- l: TestAdmin, p: jTE7cm666Xk6
INSERT INTO `users` (`id`, `login`, `password`, `role_id`) VALUES ('1', 'TestAdmin', 'DJAhPVmfV76bEZ9xsW5O3oaN9o+zmwpRZ78XW5QspToIjtbBlAFSbd5v3l/QFdj1F5svzjMZ5tuQsugny0MnpA==', '1');

-- l: TestUser, p: qAWfMuqyVEe5
INSERT INTO `users` (`id`, `login`, `password`, `role_id`) VALUES ('2', 'TestUser', '31sJZ7dGw9iFvJUqKIuS34JHj3D0MPLplLN+dxTq3vL3zz8pxkUSUCamau8UW1nGBOyNlQ0NE1NLWXYZNSV/Hg==', '2');
```

## Autentykacja i autoryzacja

Ten etap zaczniemy od dodania nowej przestrzeni nazw. Posłuży ona nam do stworzenia _łącznika_ pomiędzy bazą danych, a mechanizmem autentykacji i autoryzacji. W tym celu modyfikujemy ponownie plik `composer.json` do postaci:

```
#!json
{
    "require": {
        "doctrine/dbal": "2.2.*",
        "silex/silex": "~1.2",
        "symfony/config": "~2.3",
        "symfony/form": "~2.3",
        "symfony/locale": "~2.3",
        "symfony/security": "~2.3",
        "symfony/security-csrf": "~2.3",
        "symfony/translation": "~2.3",
        "symfony/twig-bridge": "~2.3",
        "symfony/validator": "~2.3",
        "symfony/yaml": "~2.3"
    },
    "autoload": {
        "psr-0": {
            "Controller" : "src/",
            "Form" : "src/",
            "Model" : "src/",
            "Provider" : "src/"
        }
    }
}
```

Następnie, jak zwykle aktualizujemy biblioteki.
 
W katalogu z modelami tworzymy plik `UsersModel.php` o następującej zawartości:
 
```
#!php
<?php
/**
 * Users model.
 *
 * @author EPI <epi@uj.edu.pl>
 * @link http://epi.uj.edu.pl
 * @copyright 2015 EPI
 */

namespace Model;

use Doctrine\DBAL\DBALException;
use Silex\Application;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

/**
 * Class Users.
 *
 * @category Epi
 * @package Model
 * @use Silex\Application
 */
class UsersModel
{
    /**
     * Db object.
     *
     * @access protected
     * @var Silex\Provider\DoctrineServiceProvider $db
     */
    protected $db;

    /**
     * Object constructor.
     *
     * @access public
     * @param Silex\Application $app Silex application
     */
    public function __construct(Application $app)
    {
        $this->db = $app['db'];
    }

    /**
     * Loads user by login.
     *
     * @access public
     * @param string $login User login
     * @throws UsernameNotFoundException
     * @return array Result
     */
    public function loadUserByLogin($login)
    {
        $user = $this->getUserByLogin($login);

        if (!$user || !count($user)) {
            throw new UsernameNotFoundException(
                sprintf('Username "%s" does not exist.', $login)
            );
        }

        $roles = $this->getUserRoles($user['id']);

        if (!$roles || !count($roles)) {
            throw new UsernameNotFoundException(
                sprintf('Username "%s" does not exist.', $login)
            );
        }

        return array(
            'login' => $user['login'],
            'password' => $user['password'],
            'roles' => $roles
        );

    }

    /**
     * Gets user data by login.
     *
     * @access public
     * @param string $login User login
     *
     * @return array Result
     */
    public function getUserByLogin($login)
    {
        try {
            $query = '
              SELECT
                `id`, `login`, `password`, `role_id`
              FROM
                `users`
              WHERE
                `login` = :login
            ';
            $statement = $this->db->prepare($query);
            $statement->bindValue('login', $login, \PDO::PARAM_STR);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return !$result ? array() : current($result);
        } catch (\PDOException $e) {
            return array();
        }
    }

    /**
     * Gets user roles by User ID.
     *
     * @access public
     * @param integer $userId User ID
     *
     * @return array Result
     */
    public function getUserRoles($userId)
    {
        $roles = array();
        try {
            $query = '
                SELECT
                    `roles`.`name` as `role`
                FROM
                    `users`
                INNER JOIN
                    `roles`
                ON `users`.`role_id` = `roles`.`id`
                WHERE
                    `users`.`id` = :user_id
                ';
            $statement = $this->db->prepare($query);
            $statement->bindValue('user_id', $userId, \PDO::PARAM_INT);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            if ($result && count($result)) {
                $result = current($result);
                $roles[] = $result['role'];
            }
            return $roles;
        } catch (\PDOException $e) {
            return $roles;
        }
    }
}

```

Jak widać, model ten jest odpowiedzialny za pobranie z bazy danych informacji o użytkowniku i jego rolach. 
 
Teraz przechodzimy do przygotowania wspomnianego wyżej _łącznika_. Tworzymy brakujacy katalog na klasy dla nowej przestrzeni nazw:

```
#!bash
cd ~/silex_test
mkdir -p src/Provider
```
a w nim plik `UserProvider.php` o następującej zawartości:

```
#!php
<?php
/**
 * User provider.
 *
 * @author EPI <epi@uj.edu.pl>
 * @link http://epi.uj.edu.pl
 * @copyright 2015 EPI
 */

namespace Provider;

use Silex\Application;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

use Model\UsersModel;

/**
 * Class UserProvider.
 *
 * @category Epi
 * @package Provider
 * @use Silex\Application
 * @use Symfony\Component\Security\Core\User\UserProviderInterface
 * @use Symfony\Component\Security\Core\User\UserInterface
 * @use Symfony\Component\Security\Core\User\User
 * @use Symfony\Component\Security\Core\Exception\UsernameNotFoundException
 * @use Symfony\Component\Security\Core\Exception\UnsupportedUserException
 * @use Model\UsersModel
 */
class UserProvider implements UserProviderInterface
{
    /**
     * Silex application.
     *
     * @access protected
     * @var Silex\Application $app
     */
    protected $app;

    /**
     * Object constructor.
     *
     * @access public
     * @param Silex\Application $app Silex application
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Load user by username.
     *
     * @access public
     * @param string $login User login
     *
     * @return User Result
     */
    public function loadUserByUsername($login)
    {
        $userModel = new UsersModel($this->app);
        $user = $userModel->loadUserByLogin($login);
        return new User(
            $user['login'],
            $user['password'],
            $user['roles'],
            true,
            true,
            true,
            true
        );
    }

    /**
     * Refresh user.
     *
     * @access public
     * @param UserInterface $user User
     *
     * @return User Result
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    get_class($user)
                )
            );
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Check if supports selected class.
     *
     * @access public
     * @param string $class Class name
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === 'Symfony\Component\Security\Core\User\User';
    }
}

```

Struktura klasy `UserProvider` jest zdeterminowana przez interejs jaki implementuje (`UserProviderInterface`).

Przygotujmy teraz kontoler `AuthController`, który będzie brał udział w autentykacji:

```
#!php
<?php
/**
 * Auth controller.
 *
 * @author EPI <epi@uj.edu.pl>
 * @link http://epi.uj.edu.pl
 * @copyright 2015 EPI
 */

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Form\LoginForm;

/**
 * Class AuthController.
 *
 * @package Controller
 * @implements ControllerProviderInterface
 */
class AuthController implements ControllerProviderInterface
{
    /**
     * Data for view.
     *
     * @access protected
     * @var array $view
     */
    protected $view = array();

    /**
     * Routing settings.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @return AlbumsController Result
     */
    public function connect(Application $app)
    {
        $authController = $app['controllers_factory'];
        $authController->match('login', array($this, 'loginAction'))
            ->bind('auth_login');
        $authController->get('logout', array($this, 'logoutAction'))
            ->bind('auth_logout');
        return $authController;
    }

    /**
     * Login action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function loginAction(Application $app, Request $request)
    {
        $user = array(
            'login' => $app['session']->get('_security.last_username')
        );

        $form = $app['form.factory']->createBuilder(new LoginForm(), $user)
            ->getForm();

        $this->view = array(
            'form' => $form->createView(),
            'error' => $app['security.last_error']($request)
        );

        return $app['twig']->render('auth/login.twig', $this->view);
    }

    /**
     * Logout action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function logoutAction(Application $app, Request $request)
    {
        $app['session']->clear();
        return $app['twig']->render('auth/logout.twig', $this->view);
    }
}

```

Formularz logowania `LoginForm.php` wygląda następująco:

```
#!php
<?php
/**
 * Log in form.
 *
 * @author EPI <epi@uj.edu.pl>
 * @link http://epi.uj.edu.pl
 * @copyright 2015 EPI
 */

namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class LoginForm.
 *
 * @category Epi
 * @package Form
 * @extends AbstractType
 * @use Symfony\Component\Form\AbstractType
 * @use Symfony\Component\Form\FormBuilderInterface
 * @use Symfony\Component\OptionsResolver\OptionsResolverInterface
 * @use Symfony\Component\Validator\Constraints as Assert
 */
class LoginForm extends AbstractType
{
    /**
     * Form builder.
     *
     * @access public
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return  $builder->add(
            'login',
            'text',
            array(
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Length(array('min' => 8, 'max' => 16))
                )
            )
        )
        ->add(
            'password',
            'password',
            array(
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Length(array('min' => 8))
                )
            )
        );
    }

    /**
     * Gets form name.
     *
     * @access public
     *
     * @return string
     */
    public function getName()
    {
        return 'loginForm';
    }
}

```

Brakujący widok `auth/login.twig`:

```
#!twig
{% extends 'base.twig' %}

{% block title %}{{ 'Log in'|trans }}{% endblock %}

{% block content %}
    <h1>
        {{ 'Login'|trans }}
    </h1>
    {{ error }}
    <form action="{{ path('auth_login_check') }}" method="post">
        {{ form_widget(form) }}
        <input type="submit" name="submit" value="{{ 'Login'|trans }}" />
    </form>
{% endblock %}
```

Pozostaje jeszcze w pliku `base.twig` dodać odnośniki do logowania i wylogowania użytkownika:

```
#!twig
{# ... #}
<nav>
    {% if is_granted('IS_AUTHENTICATED_FULLY') %}
        <a href="{{ url('auth_logout') }}" title="{{ 'Logout'|trans }}">{{ 'Logout'|trans }}</a>
    {% else %}
        <a href="{{ url('auth_login') }}" title="{{ 'Login'|trans }}">{{ 'Login'|trans }}</a>
    {% endif %}
</nav>
{# ... #}
```

Przechodzimy teraz do pliku `index.php` aby przeprowadzić konfigurację zabezpiczeń. W pierwszej kolejności sprawdzamy czy zarejestrowany jest Silex\Provider\SessionServiceProvider`. Jeżeli nie, dodajemy linię:

```
#!php
<?php
// ...
$app->register(new Silex\Provider\SessionServiceProvider());
// ...
```

Montujemy nowy kontroler:

```
#!php
<?php
// ...
$app->mount('auth', new Controller\AuthController());
// ...
```

i podmieniamy reguły `firewall`'a na:

```
#!php
<?php
// ....
$app->register(
    new Silex\Provider\SecurityServiceProvider(),
    array(
        'security.firewalls' => array(
            'admin' => array(
                'pattern' => '^.*$',
                'form' => array(
                    'login_path' => 'auth_login',
                    'check_path' => 'auth_login_check',
                    'default_target_path'=> '/albums/1',
                    'username_parameter' => 'loginForm[login]',
                    'password_parameter' => 'loginForm[password]',
                ),
                'anonymous' => true,
                'logout' => array(
                    'logout_path' => 'auth_logout',
                    'default_target_path' => 'albums_index'
                ),
                'users' => $app->share(
                    function() use ($app)
                    {
                        return new Provider\UserProvider($app);
                    }
                ),
            ),
        ),
        'security.access_rules' => array(
            array('^/auth.+$', 'IS_AUTHENTICATED_ANONYMOUSLY'),
            array('^/.+$', 'ROLE_ADMIN')
        ),
        'security.role_hierarchy' => array(
            'ROLE_ADMIN' => array('ROLE_USER'),
        ),
    )
);
// ...
```

Uwagi:

* W powyższej konfiguracji tylko użytkownik o roli `ROLE_ADMIN` ma dostęp do systemu.
* Do otwarcia dowolnej strony aplikacji konieczne jest logowanie.
* `login_path` - *ZAWSZE MUSI BYĆ* zdefiniowana poza obszarem chronionym lub w chronionym ale z dopuszczeniem anonimowego dostępu.
* `check_path` - *MUSI BYĆ* zdefiniowana w obszarze chronionym.

## Ćwiczenie I

W jaki sposób można umożliwić pełny dostęp do aplikacji użytkownikowi i roli `ROLE_USER`?

## Ćwiczenie II

Ogranicz mechanizmy dodawania, edycji i usuwania elementu do użytkownika o roli `ROLE_ADMIN`. Pamiętaj, że sprawdzenie uprawnień należy zrobić zarówno w szablonie listy elementów, jak również wewnątrz konkretnej akcji.

## Ćwiczenie III

Zmodyfikuj reguły `firewall`'a tak, aby lista elementów była dostępna dla użytkownika anonimowego.
