# Silex I - wprowadzenie

## Instalacja na wierzbie

W katalogu domowym użytkownika na wierzbie tworzymy katalog, w którym umieszczone zostaną wszystkie pliki projektu. Roboczo nazwałem go `silex_test`:

```
#!bash
cd ~
mkdir silex_test
```

Następnie tworzymy plik `composer.json`

```
#!bash
cd silex_test
vim composer.json
```

 o następującej zawartości:

```
#!json
{
    "require": {
        "silex/silex": "~1.2"
    }
}
```

Przy użyciu menedżera pakietów o nazwie _Composer_ pobieramy komponenty niezbędne do działania _Silex_:

```
#!bash
curl -sS https://getcomposer.org/installer | php
php composer.phar install
```

W katalogu projektu tworzymy dodatkowe katalogi:

```
cd ~/silex_test
mkdir web src
```

Pozostaje w katalogu `web` utworzyć plik `index.php` o następującej zawartości:

```
#!php
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', E_ALL);
require_once dirname(dirname(__FILE__)) . '/vendor/autoload.php';
$app = new Silex\Application();
$app->run();

```

i zrobić w katalogu `public_html` dowiązanie symboliczne do katalogu `web`:

```
#!bash
cd ~/public_html
ln -s ~/silex_test/web/ silex_test
```

Pozostaje uruchomić w przeglądarce adres: `http://wierzba.wzks.uj.edu.pl/~USER/silex_test/`

## Debuger

Skrypt `index.php` modyfikujemy następująco:

```
#!php
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', E_ALL);
require_once dirname(dirname(__FILE__)) . '/vendor/autoload.php';
$app = new Silex\Application();
$app['debug'] = true;
$app->run();
```

i ponownie uruchamiamy aplikację w przeglądarce.

## Hello World!

W katalogu `web` tworzymy plik `.htaccess`:

```
#!bash
cd ~/silex_test
vim web/.htaccess
```

o następującej zawartości:

```
#!bash
<IfModule mod_rewrite.c>
    Options -MultiViews
    RewriteEngine On
    RewriteBase /~USER/silex_test
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [QSA,L]
</IfModule>

```

Skrypt `index.php` modyfikujemy następująco:

```
#!php
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', E_ALL);
require_once dirname(dirname(__FILE__)) . '/vendor/autoload.php';
$app = new Silex\Application();
$app['debug'] = true;
$app->get('/hello/{name}', function ($name) use ($app) {
    return 'Hello '.$app->escape($name);
});
$app->run();
```

W przeglądarce przechodzimy pod adres: `http://wierzba.wzks.uj.edu.pl/~USER/silex_test/hello/World!`.


## Przykład I

Skrypt `index.php` modyfikujemy następująco:

```
#!php
<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', E_ALL);

require_once dirname(dirname(__FILE__)) . '/vendor/autoload.php';

$app = new Silex\Application();

$app['debug'] = true;

$data = array(
    0 => array(
        'name' => 'John',
        'email' => 'john@example.com',
    ),
    1 => array(
        'name' => 'Mark',
        'email' => 'mark@example.com',
    ),
);

$app->get('/data', function () use ($data) {
    $view = '';
    foreach ($data as $row) {
        $view .= $row['name'];
        $view .= ' : ';
        $view .= $row['email'];
        $view .= '<br />';
    }
    return $view;
});

$app->run();
```

## Przykład II

Skrypt `index.php` modyfikujemy następująco:

```
#!php
<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', E_ALL);

require_once dirname(dirname(__FILE__)) . '/vendor/autoload.php';

$app = new Silex\Application();

$app['debug'] = true;

$data = array(
    0 => array(
        'name' => 'John',
        'email' => 'john@example.com',
    ),
    1 => array(
        'name' => 'Mark',
        'email' => 'mark@example.com',
    ),
);

$app->get('/data', function () use ($data) {
    $view = '';
    foreach ($data as $row) {
        $view .= $row['name'];
        $view .= ' : ';
        $view .= $row['email'];
        $view .= '<br />';
    }
    return $view;
});

$app->get('/data/{id}', function (Silex\Application $app, $id) use ($data) {
    if (!isset($data[$id])) {
        $app->abort('404', 'Invalid entry');
    } else {
        $view = '';
        $view .= $data[$id]['name'];
        $view .= ' : ';
        $view .= $data[$id]['email'];
    }
    return $view;
});

$app->run();
```

Uruchomienie poprzez adres: `http://wierzba.wzks.uj.edu.pl/~USER/silex_test/data/1` lub `http://wierzba.wzks.uj.edu.pl/~USER/silex_test/data/2`.

## Szablony Twig

Tworzymy katalog na pliki szablonów:

```
#!bash
cd ~/silex_test
mkdir -p src/views
```

Modyfikujemy plik `composer.json`:

```
#!bash
vim composer.json
```

następująco:

```
#!json
{
    "require": {
        "silex/silex": "~1.2",
        "symfony/twig-bridge": "~2.3"
    }
}
```
a następnie aktualizujemy zewnętrzne blblioteki:

```
#!bash
php composer.phar update
```

Modyfikujemy `index.php`:

```
#!php
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', E_ALL);

require_once dirname(dirname(__FILE__)) . '/vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => dirname(dirname(__FILE__)) . '/src/views',
));

$app->get('/hello/{name}', function ($name) use ($app) {
    return $app['twig']->render('hello.twig', array(
        'name' => $name,
    ));
});
$app->run();
```

Tworzymy nowy szablon:

```
#!bash
vim src/views/hello.twig
```

o następującej zawartości:

```
#!twig
<h1>
   Hello  {{ name }}!
</h1>
```













