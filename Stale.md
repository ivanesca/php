# Stałe

Stałe tworzymy przy użyciu funkcji `define()`. Stałe nie mogą zostać zmienione podczas działania skryptu. Stałymi mogą być dane typów skalarnych.

```

#!php
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
define('CONSTANT', 'Some constant');
echo CONSTANT;
echo Constant;

```

## Stałe magiczne

* `__LINE__` - aktualna linia w pliku,
* `__FILE__` - pełna ścieżka do pliku,
* `__DIR__` - ścieżka do katalogu zawierajacego plik, równoważne wywołaniu `dirname(__FILE__)`,
* `__FUNCTION__` - nazwa funkcji,
* `__CLASS__` - nazwa klasy,
* `__METHOD__` - nazwa metody,
* `__NAMESPACE__` - nazwa przestrzeni nazw.

```

#!php
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
echo __LINE__ . "\r\n";
echo __FILE__ . "\r\n";
echo __DIR__ . "\r\n";
echo dirname(dirname(__FILE__)) . "\r\n";

```




