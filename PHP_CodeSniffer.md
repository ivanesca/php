# PHP_CodeSniffer

`PHP_CodeSniffer` jest narzędziem pozwalajacym na sprawdzenie czy tworzony przez nas kod spełnia wybrany standard dla kodu. Strona główna projektu znajduje się pod adresem https://github.com/squizlabs/PHP_CodeSniffer, dokumentacja https://github.com/squizlabs/PHP_CodeSniffer/wiki

Instalacja, jak zwykle wymaga pobrania stosownego archiwum `PHAR`:

```
#!bash
cd ~/silex_test
curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
```

Spawdzenie dostępnych opcji:

```
#!bash
php phpcs.phar -h
```

Sprawdzenie dostępnch standardów kodowania:

```
#!bash
php phpcs.phar -i
```

Domyślny standard według możemy ustawić na przykład tak:

```
#!bash
php phpcs.phar --config-set default_standard PSR2
```

Konfiguracja `PHP_CodeSniffer` znajduje się w pliku `CodeSniffer.conf`.

Sprawdzenie kodu możemy przeprowadzić poleceniami:

* dla katalogu:

```
!#bash
php phpcs.phar src
```

* dla pojedynczego pliku:

```
#!bash
php phpcs.phar src/Form/AlbumForm.php
```

Przykładowy wynik sprawdzania poprawności może wyglądać tak:

```
#!bash
FILE: silex_test/src/Form/AlbumForm.php
----------------------------------------------------------------------
FOUND 4 ERRORS AFFECTING 4 LINES
----------------------------------------------------------------------
 42 | ERROR | [x] Only one argument is allowed per line in a
    |       |     multi-line function call
 51 | ERROR | [x] Only one argument is allowed per line in a
    |       |     multi-line function call
 60 | ERROR | [x] Only one argument is allowed per line in a
    |       |     multi-line function call
 83 | ERROR | [x] The closing brace for the class must go on the next
    |       |     line after the body
----------------------------------------------------------------------
PHPCBF CAN FIX THE 4 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------

Time: 30ms; Memory: 4.75Mb
```

Kod może zostać automatycznie poprawiony przy użyciu [PHP Code Beautifier and Fixer](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Fixing-Errors-Automatically "PHP Code Beautifier and Fixer"). 

Instalacja jest podobna do instalacji `PHP_CodeSniffer`'a:

```
#!bash
cd ~/silex_test
curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar
```