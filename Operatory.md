# Operatory

Priorytet operatorów


Priotytety operatorów:

| **Łączność** | **Operator** | **Dodatkowe informacje**  |
| :-------------: |:-------------:| :-----:|
| brak | clone, new | tworzenie obiektów |
| z lewej | *[* | tablice |
| z prawej | ** | arytmetyczny |
| z prawej | ++ -- ~ (int) (float) (string) (array) (object) (bool) @ | typy zmiennych, operatory inkrementacji dekrementacji |
| brak | instanceof | typy obiektów
| z prawej | ! | logiczny |
| z lewej | * / % | arytmetyczny |
| z lewej | + - . | arytmetyczny, ciągi |
| z lewej | << >> | bitowy |
| brak | < <= > >= | porównania |
| brak |	== != === !== <> | porównania |
| z lewej | & | bitowy, referencje |
| z lewej | ^ | bitowy |
| z lewej | &#124; | bitowy |
| z lewej |&& | logiczny |
| z lewej | &#124; &#124;| logiczny |
| z lewej | ? :	| tenarny |
| z prawej | = += -= *= **= /= .= %= &= |= ^= <<= >>= => | przypisanie |
| z lewej | and	| logiczny |
| z lewej | xor | logiczny |
| z lewej | or | logiczny |
| z lewej | , |	|

[Szczególowy opis operatorów](http://php.net/manual/en/language.operators.php)