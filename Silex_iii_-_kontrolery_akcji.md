# Silex III - kontrolery akcji

## Wstęp

W kolejnym etapie budowy aplikacji opartej na Silex'ie uporządkujemy strukturę pliku `index.php` i wprowadzimy _kontrolery akcji_, do których przeniesiemy całą logikę aplikacji. Aby wprowadzić kontrolery akcji, będziemy musieli podzielić naszą aplikację części, w których każdy z kontrolerów będzie operował. Zrobimy to wyróżniając w aplikacji rzeczowniki, z którymi będziemy wiązać operacje jakie będziemy mogli na nich wykonać czyli czasowniki. Dla przykładu, w aplikacji bloga, rzeczownikami (w liczbie mnogiej) będą:

* Posts
* Comments
* Tags
* Categories
* Users

Operacje, jakie będą wykonywane opisane będą przez czasowniki. Dla przykładu, w aplikacji bloga będą to:

 * view
 * add
 * edit
 * delete
 * index
 
Wszystkie operacje będzie trzeba związać z rzeczownikami. 

Kontrolery akcji, będą zatem rzeczownikami czyli obszarami w aplikacji, natomiast akcje, to wszystkie operacje (czasowniki) jakie będzie można wykonać w obrębie kontrolerów. Od strony technicznej, kontroler to klasa, a operacje, to po prostu metody publiczne w tej klasie. 

Dla porządku wprowadźmy jeszcze dodatkową konwencję. Ścieżki routowania, w ujęciu kontrolerów i akcji zapiszemy następująco: `http://example.com/controller/action/param1/param2`. Korzystając z powyższego przykłądu bloga URL `http://example.com/posts/view/1` oznacza odwołanie do akcji `viewAction()`, kontrolera `PostsController` i wyświetlenie rekordu o id `1`. 

Na potrzeby struktury kodu, w katalogu `src` utworzymy katalog `Controller`, w którym umieścimy wszystkie kontrolery, a w katalogu `src/views` utworzymy katalogi (pisane małymi literami, będącymi nazwami kontrolerów, a wewnątrz nich pliki szablonów o nazwie takiej jak nazwa akcji, której dany szablon odpowiada. I znów, dla `http://example.com/posts/view/1` w katalogu `Controller` powstanie plik `PostsController.php` z klasą `PostsController` i akcją `viewAction()`, a w katalogu z szablonami `src/views` powstanie katalog `posts`, a w nim plik `view.twig`.

Zanotujmy, kolejną konwencję, że URL: `http://example.com/posts` lub `http://example.com/posts/` ma odpowiadać akcji `indexAction()`, kontrolera `PostsController`, a URL `http://example.com/` lub `http://example.com` ma odpowiadać akcji `indexAction()` kontrolera `IndexController`.

## Struktura katalogów

Utwórzmy niezbędne katalogi:

```
#!bash
mkdir -p ~/silex_test/src/Controller
```

## Hello World! - raz jeszcze

Dla przypomnienia URL dla `http://example.com/hello/World!` oznacza wedle nowej konwencji, że `hello` odpowiada kontrolerowi i wywoływania jest jego domyślna akcja `index`, a `World!` oznacza przekazywany parametr. Na powyższy URL możemy też patrzeć jako na akcję `hello` kontrolera `index`. Dla ustalenia uwagi przyjmę tą pierwszą interpretację.

Utwórzmy brakujący katalog na akcje:

```
#!bash
mkdir -p ~/silex_test/src/views/hello
```

i przenieśmy do niego przygotowany już wcześniej szablon `hello.twig` zmieniajc jego nazwę na `index.twig`.

W katalogu `~/silex_test/src/Controller` należy teraz utworzyć plik `HelloController.php` o następującej zawartości:

```
<?php
/**
 * Hello controller.
 *
 * @link http://epi.uj.edu.pl
 * @author epi(at)uj(dot)edu(dot)pl
 * @copyright EPI 2015
 */

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class HelloController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @return HelloController Result
     */
    public function connect(Application $app)
    {
        $helloController = $app['controllers_factory'];
        $helloController->get('/{name}', array($this, 'indexAction'));
        return $helloController;
    }

    /**
     * Index action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function indexAction(Application $app, Request $request)
    {
        $view = array();
        $view['name'] = (string)$request->get('name', '');
        return $app['twig']->render('hello/index.twig', $view);
    }

}
```

Teraz musimy w pliku `index.php` zarejestrować kontroler, a zatem:

```
#!bash
vim ~/silex_test/web/index.php
```

```
#!php
<?php
// ...
$app->mount('/hello/', new Controller\HelloController());
// ...
```

Należy pamiętać aby usunąć z kodu `index.php`:

```
#!php
$app->get('/hello/{name}', function ($name) use ($app) {
    return 'Hello '.$app->escape($name);
});
```

Na koniec zostaje poinformowanie mechanizmu automatycznego ładowania klas, o tym gdzie ma szukać naszych kontrolerów. Otwieramy plik `composer.json` i modyfikujemy następująco:

```
#!json
{
    "require": {
        "silex/silex": "~1.2",
        "symfony/twig-bridge": "~2.3"
    },
    "autoload": {
        "psr-0": {
            "Controller" : "src/"
        }
    }
}
```
i wykonać polecenie:

```
#!bash
cd ~/silex_test
php composer.phar update
```

Przy okazji proszę zwrócić uwagę, że Silex samodzielnie zadbać o aktualizację zainstalowanych bibliotek, np tak:

```
#!bash
Loading composer repositories with package information
Updating dependencies (including require-dev)
  - Removing silex/silex (v1.2.3)
  - Installing silex/silex (v1.2.4)
    Downloading: 100%         

  - Removing twig/twig (v1.18.0)
  - Installing twig/twig (v1.18.1)
    Downloading: 100%         

Writing lock file
Generating autoload files
```

Teraz można uruchomić aplikację w przeglądarce.


## Ćwiczenie I

Mechanizm z poprzednich zajęć operujący na tablicy `$data`:

```
#!php
<?php
// ...
$data = array(
    0 => array(
        'name' => 'John',
        'email' => 'john@example.com',
    ),
    1 => array(
        'name' => 'Mark',
        'email' => 'mark@example.com',
    ),
);
// ...
```

przerobić na język kontrolerów akcji.

Tablice z danymi można umieścić tymczasowo w kontrolerze `DataController.php` w następujący sposób (dodając metodę prywatną `_getData()`:

```
#!php
<?php
// ...

    private function _getData()
    {
        return array(
           0 => array(
               'name' => 'John',
               'email' => 'john@example.com',
           ),
           1 => array(
               'name' => 'Mark',
               'email' => 'mark@example.com',
           ),
       );
    
    }

// ...
```

## Ćwiczenie II

Mechanizm kursów walut, korzystający z [z danych](oddzielanie_logiki_aplikacji_od_wygladu/Dane_zrodlowe) proszę przerobić na język kontrolerów akcji. Dla ustalenia uwagi:

* URL dla listy wszystkich kursów będzie wyglądał tak: `http://example.com/currencies/`, `http://example.com/currencies`, `http://example.com/currencies/index` lub `http://example.com/currencies/index/`
* URL dla wyświetlenia pojedynczych rekordów będzie wyglądał tak: `http://example.com/currencies/view/HUF` (HUF - konkretny kod waluty),
* Kontroler to `CurrenciesController`.

## Odnośniki wewnątrz aplikacji

Aby móc w sposób łatwy zarządzać i korzystać z odnośników wewnątrz aplikacji, np na liście wszystkich rekordów zrobić odnośniki do wyświetlenia pojedynczego rekordu:

 * zarejestrować w `index.php` nową usługę: `$app->register(new Silex\Provider\UrlGeneratorServiceProvider());`
 * nadać nazwy odnośnikom w metodzie `connect()` kontrolera:
 
```
#!php
public function connect(Application $app)
{
 $dataController = $app['controllers_factory'];
 $dataController->get('/', array($this, 'indexAction'))->bind('data_index');
 $dataController->get('/view/{id}', array($this, 'viewAction'))->bind('data_view');
 return $dataController;
}
```
 * w szablonie utworzyć odnośnik następująco:
 
```
#!twig
<a href="{{ url('data_view', {'id': data.id}) }}" title="View data">View data</a>
``` 

## Ćwiczenie III 
   
W mechanizmie z poprzednich zajęć operującym na tablicy `$data`, na liście wszystkich wpisów dodać odnośniki pozwalajace na wyświetlanie pojedynczych rekordów.
   
## Ćwiczenie IV
   
Na liście kursów walut proszę dodać odnośniki pozwalajace na wyświetlanie pojedynczego rekordu.
   
## Przykładowy szablon struktury

Dla postów przykładowy szablon kontrolera może wyglądać tak:

```
#!bash
vim ~/silex_test/src/Controller/PostsController.php
```

```
#!php
<?php
/**
 * Posts controller.
 *
 * @link http://epi.uj.edu.pl
 * @author epi(at)uj(dot)edu(dot)pl
 * @copyright EPI 2015
 */

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class PostsController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @access public
     * @param Silex\Application $app Silex application
     */
    public function connect(Application $app)
    {
        $postsController = $app['controllers_factory'];
        $postsController->get('/', array($this, 'indexAction'));
        $postsController->get('/add', array($this, 'addAction'));
        $postsController->get('/edit/{id}', array($this, 'editAction'));
        $postsController->get('/delete/{id}', array($this, 'deleteAction'));
        $postsController->get('/view{id}', array($this, 'viewAction'));
        return $postsController;
    }

    /**
     * Index action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function indexAction(Application $app, Request $request)
    {
        $view = array();
        return $app['twig']->render('posts/index.twig', $view);
    }

    /**
     * Add action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function addAction(Application $app, Request $request)
    {
        $view = array();
        return $app['twig']->render('posts/add.twig', $view);
    }

    /**
     * Edit action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function editAction(Application $app, Request $request)
    {
        $view = array();
        return $app['twig']->render('posts/edit.twig', $view);
    }

    /**
     * Delete action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function deleteAction(Application $app, Request $request)
    {
        $view = array();
        return $app['twig']->render('posts/delete.twig', $view);
    }

    /**
     * View action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function viewAction(Application $app, Request $request)
    {
        $view = array();
        return $app['twig']->render('posts/view.twig', $view);
    }

}
```

Następnie należy przygotować puste szablony dla poszczególnych akcji.

Zarejestrowanie kontrolera:

```
#!bash
vim ~/silex_test/web/index.php
```

```
#!php
<?php
// ...
$app->mount('/posts/', new Controller\PostsController());
// ...
```

Pozostaje aktualizacja ładowania klas:

```
#!bash
cd ~/silex_test
php composer.phar update
```

Po tych zmianach można spróbować uruchomić w przeglądarce następujące adresy:

```
#!bash
http://wierzba.wzks.uj.edu.pl/~USER/silex_test/posts/index
http://wierzba.wzks.uj.edu.pl/~USER/silex_test/web/posts/add
http://wierzba.wzks.uj.edu.pl/~USER/silex_test/web/posts/edit/123
http://wierzba.wzks.uj.edu.pl/~USER/silex_test/web/posts/delete/123
http://wierzba.wzks.uj.edu.pl/~USER/silex_test/web/posts/view/123
```


