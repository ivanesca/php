# Oddzielanie logiki aplikacji od wyglądu

## Przygotowanie

Skrypt `data.inc.php` z danymi źródłowymi znajduje się [tutaj](Dane_zrodlowe). Plik należy umieścić w katalogu `lib` znajdującym się w katalogu głównym aplikacji. W katalogu głównym utwórzmy skrypt `currencies.php` o następującej zawartości:

```
#!php
<?php
/**
 * Demo script.
 *
 * @link http://epi.uj.edu.pl
 */

require dirname(__FILE__) . '/lib/dev.inc.php';
require dirname(__FILE__) . '/lib/data.inc.php';

$data = data_loader();
```

## Wypisanie danych na ekranie I

Wypiszmy dane na ekranie w postaci tablicy umieszczając cały kod w skrypcie `currencies.php`.

## Wypisanie danych na ekranie II

Tym razem przenieśmy wypisanie danych na ekranie do osobnego skryptu. W katalogu głównym utwórzmy podkatalog `templates`, w którym umieścimy skrypt `currencies.tpl.php` o następującej zawartości:

```
#!php
<?php
/**
 * Currencies template.
 *
 * @link http://epi.uj.edu.pl
 */
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Currencies</title>
</head>
<body>
    <?php if (isset($data)): ?>
        <table>
            <thead>
                <tr>
                    <th>Kod waluty</th>
                    <th>Kurs średni</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $row): ?>
                    <tr>
                        <td><?php echo $row['kod_waluty']; ?></td>
                        <td><?php echo $row['kurs_sredni']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Brak danych</p>
    <?php endif; ?>
</body>
</html>
```
Skrypt `currencies.php` przyjmie postać:

```
#!php
<?php
/**
 * Demo script.
 *
 * @link http://epi.uj.edu.pl
 */

require dirname(__FILE__) . '/lib/dev.inc.php';
require dirname(__FILE__) . '/lib/data.inc.php';

$data = data_loader();

require dirname(__FILE__) . '/templates/currencies.tpl.php';
```

## Wypisanie danych na ekranie III

Kolejnym krokiem jest przygotowanie funkcji ładującej szablony. W katalogu `lib` tworzymy nową bibliotekę: `templates.inc.php` o następującej zawartości:

```
#!php
<?php
/**
 * Templates support.
 *
 * @link http://epi.uj.edu.pl
 */

/**
 * Display template.
 *
 * @param array $template Data for template
 * @param string $name Template name
 */
function display_template($template, $name = 'index')
{
    $extension = '.tpl.php';
    $template_dir = dirname(dirname(__FILE__)) . '/templates';

    if (!file_exists($template_dir . '/' . $name . $extension)) {
        $name = '404';
    }
    require $template_dir . '/' . $name . $extension;
}
```

Dodatkowo, w katalogu `templates` utwórzmy domyślny szablon na wypadek braku wybranego szablonu (plik `404.tpl.php`):
  
```
#!php
<?php
/**
 * Default 404 template.
 *
 * @link http://epi.uj.edu.pl
 */
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>404 error!</title>
</head>
<body>
    404 error!
</body>
</html>
```

Zmodyfikujmy skrypt `currencies.php` następująco:

```
#!php
<?php
/**
 * Demo script.
 *
 * @link http://epi.uj.edu.pl
 */

require dirname(__FILE__) . '/lib/dev.inc.php';
require dirname(__FILE__) . '/lib/data.inc.php';
require dirname(__FILE__) . '/lib/templates.inc.php';

$template = array();

$template['data'] = data_loader();
$template['layout'] = 'currencies';
$template['title'] = 'Currencies';

display_template($template, $template['layout']);
```

oraz szablon `currencies.tpl.php`:

```
#!php
<?php
/**
 * Currencies template.
 *
 * @link http://epi.uj.edu.pl
 */
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title><?php echo $template['title']; ?></title>
</head>
<body>
    <?php if (isset($template['data'])): ?>
        <table>
            <thead>
                <tr>
                    <th>Kod waluty</th>
                    <th>Kurs średni</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($template['data'] as $row): ?>
                    <tr>
                        <td><?php echo $row['kod_waluty']; ?></td>
                        <td><?php echo $row['kurs_sredni']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p>Brak danych</p>
    <?php endif; ?>
</body>
</html>
```
## Wypisanie danych na ekranie IV

Ostatnim krokiem, jest zmodyfikowanie tak mechanizmu szablonów aby można było korzystać z tej samej skórki dla wielu skryptów.

Skrypt `404.tpl.php`:

```
<?php
/**
 * Default 404 template.
 *
 * @link http://epi.uj.edu.pl
 */
?>
<p>
    404 error!
</p>
```

Skrypt `index.tpl.php`:

```
#!php
<?php
/**
 * Layout template.
 *
 * @link http://epi.uj.edu.pl
 */
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title><?php echo $template['title']; ?></title>
</head>
<body>
    <?php if (!file_exists($template['template_dir'] . '/' . $template['template'] . $template['extension'])): ?>
    <?php $template['template'] = '404'; ?>
    <?php endif; ?>
    <?php require $template['template_dir'] . '/' . $template['template'] . $template['extension']; ?>
</body>
</html>
```

Skrypt `currencies.tpl.php`:

```
#!php
<?php
/**
 * Currencies template.
 *
 * @link http://epi.uj.edu.pl
 */
?>
<?php if (isset($template['data'])): ?>
    <table>
        <thead>
            <tr>
                <th>Kod waluty</th>
                <th>Kurs średni</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($template['data'] as $row): ?>
                <tr>
                    <td><?php echo $row['kod_waluty']; ?></td>
                    <td><?php echo $row['kurs_sredni']; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <p>Brak danych</p>
<?php endif; ?>

```

Skrypt `templates.inc.php`:

```
#!php
<?php
/**
 * Templates support.
 *
 * @link http://epi.uj.edu.pl
 */

/**
 * Display template.
 *
 * @param array $template Data for template
 * @param string $layout Template name
 */
function display_template($template, $layout = 'index')
{
    $template['extension'] = '.tpl.php';
    $template['template_dir'] = dirname(dirname(__FILE__)) . '/templates';

    if (!file_exists(
        $template['template_dir'] . '/' . $layout . $template['extension']
    )) {
        $layout = '404';
    }
    require $template['template_dir'] . '/' . $layout . $template['extension'];
}

```

Skrypt `currencies.php`:

```
#!php
<?php
/**
 * Demo script.
 *
 * @link http://epi.uj.edu.pl
 */

require dirname(__FILE__) . '/lib/dev.inc.php';
require dirname(__FILE__) . '/lib/data.inc.php';
require dirname(__FILE__) . '/lib/templates.inc.php';

$template = array();

$template['data'] = data_loader();
$template['layout'] = 'index';
$template['template'] = 'currencies';
$template['title'] = 'Currencies';

display_template($template, $template['layout']);
```

## Ćwiczenie I

Obsługę formularza z poprzednich zajęć wraz z odebraniem danych przesłanych metodą POST umieść w jednym skrypcie.

```
#!php
<?php
/**
 * Get data.
 *
 * @link http://epi.uj.edu.pl
 */

require dirname(__FILE__) . '/lib/dev.inc.php';
require dirname(__FILE__) . '/lib/data_manipulation.inc.php';
require dirname(__FILE__) . '/lib/templates.inc.php';

$template = array();

// ...

if (isset($_POST) && count($_POST)) {
// formularz wysłany
} else {
// wyświetlamy formularz po raz pierwszy
}

display_template($template, $template['layout']);
```

## Ćwiczenie II

Zmodyfikuj skrypt z poprzedniego ćwiczenia w taki sposób, aby wysłany formularz został wyświetlony ponownie, uzupełniony danymi wprowadzonymi przez użytkownika.

