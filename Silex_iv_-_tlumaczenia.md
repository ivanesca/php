# Silex IV - tłumaczenia

Dokumentacja dotycząca mechanizmu tłumaczeń w Silex dostępna jest [tutaj](http://silex.sensiolabs.org/doc/providers/translation.html "TranslationServiceProvider"). Za cały proces odpowiedzialny jest `TranslationServiceProvider`, który podobnie jak `Twig` działa na zasadzie usługi.

Aby uruchomić mechanizm tłumaczeń w aplikacji Silex zaczynamy od załadowania niezbędnych bibliotek. W tym celu modyfikujemy plik `composer.json` następująco:

```
#!json
{
    "require": {
        "silex/silex": "~1.2",
        "symfony/config": "~2.3",
        "symfony/translation": "~2.3",
        "symfony/twig-bridge": "~2.3",
        "symfony/yaml": "~2.3"
    },
    "autoload": {
        "psr-0": {
            "Controller" : "src/"
        }
    }

}
```

Jak zwykle, po modyfikacji `composer.json` uruchamiamy polecenie:

```
#!bash
php composer.phar update
```

Kolejnym etapem jest utworzenie struktury pod pliki tłumaczeń i utworzenie pliku pod tłumaczenia dla języka polskiego:

```
#!bash
cd ~/silex_test
mkdir -p config/locales
touch config/locales/pl.yml
```

W pliku `index.php` rejestrujemy mechanizm tłumaczeń następująco:

```
#!php
<?php
// ... 
use Symfony\Component\Translation\Loader\YamlFileLoader;

// ...

$app->register(
    new Silex\Provider\TranslationServiceProvider(), array(
        'locale' => 'pl',
        'locale_fallbacks' => array('pl'),
    )
);

$app['translator'] = $app->share($app->extend('translator', function($translator, $app) {
    $translator->addLoader('yaml', new YamlFileLoader());
    $translator->addResource('yaml', dirname(dirname(__FILE__)) . '/config/locales/pl.yml', 'pl');
    return $translator;
}));

// ...

```

## Przykład I

W pliku z tłumaczeniami `pl.yml` dodajemy wpis:

```
#!yml
Hello: Ahoj
```

następnie aby skorzystać z tłumaczenia, wystarczy plik `src/views/hello/index.twig` zmodyfikować następująco:
 
```
#!twig
<h1>
   {{ 'Hello'|trans }}  {{ name }}!
</h1>
```

## Przykład II

Tłumaczone frazy pozwalają również na przekazywanie parametrów. W pliku `pl.yml` dodajemy wpis:

```
#!yml
Hello %name%!: Ahoj %name%!
```

a następnie plik `src/views/hello/index.twig` modyfikujemy następująco:

```
#!twig
<h1>
  {{ 'Hello %name%!'|trans({'%name%': name}) }}
</h1>
```


