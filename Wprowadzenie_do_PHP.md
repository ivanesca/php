# Wprowadzenie do PHP

## Hello World!

Przygodę zaczniemy od napisania standardowego skryptu _Hello World!_. Tworzymy nowy plik o nazwie `hello.php` i następującej zawartości:


```
#!php

<?php
echo 'Hello World!';

```

Skrypt uruchamiamy w terminalu/konsoli następująco:


```
#!bash

php hello.php

```

## Stosowanie apostrofów i cudzysłowów


```
#!php

<?php
echo 'Hello World!';
echo '\r\n';
echo "\r\n";

```

Uruchommy skrypt w terminalu, tak jak poprzednio i zobaczmy czym różni się przetwarzanie dwóch nowododanych linii. Dla lepszego efektu zmodyfikujmy skrypt następująco:

```
#!php

<?php
echo 'Hello World!';
echo '\r\n';
echo "\r\n";
$counter = 1;
echo 'Counter: $counter';
echo "\r\n";
echo "Counter: $counter";
echo "\r\n";

```

Jak widać, parser w inny sposób przetwarza ciąg tekstowy ujęty w apostrofy, a inaczej w cudzysłowy. W przypadku cudzysłowów, ciąg jest przetwarzany celem ewentualnych podstawień zmiennych, w przypadku apostrofów, jest od razu wysyłany na wyjście (terminal, przeglądarka). 

__W PHP tylko w bardzo szczególnych sytuacjach powinniśmy używać cudzysłowów zamiast apostrofów, gdyż wpływa to na wydajność aplikacji.__

Dodajmy jeszcze jednen sposób wypisania statycznego ciągu i zmiennej (najbardziej poprawny):

```
#!php

<?php
echo 'Hello World!';
echo '\r\n';
echo "\r\n";
$counter = 1;
echo 'Counter: $counter';
echo "\r\n";
echo "Counter: $counter";
echo "\r\n";
echo 'Counter: ' . $counter . "\r\n";

```

W tym wypadku do łączenia ciągów użyliśmy operatora konkatenacji czyli kropki.

## Uruchamianie skryptów w przeglądarce

Aby uruchomić nasz skrypt w przeglądarce wystarczy skopiować go na _wierzbę_ do katalogu `public_html`, a następnie przejść w przeglądarce pod adres `http://wierzba.wzks.uj.edu.pl/~[USER]/hello.php`. W miejsce `[USER]` należy wstawić swoją nazwę użytkownika na _wierzbie_.
 
Poprawmy kod w skrypcie, tak aby wszystkie przełamania linii były widoczne w przeglądarce:

```
#!php

<?php
echo '<pre>';
echo 'Hello World!';
echo '\r\n';
echo "\r\n";
$counter = 1;
echo 'Counter: $counter';
echo "\r\n";
echo "Counter: $counter";
echo "\r\n";
echo 'Counter: ' . $counter . "\r\n";
echo '</pre>';

```

Po ponownym uruchomieniu skryptu w przeglądarce proszę zobaczyć jak wygląda źródło strony. Przy okazji pokazałem jak można umieszczać kod HTML w skrypcie PHP. Nie jest to dobry spsób, bo kolorowanie składni w edytorze traktuje tak wstawiony HTML jako zwykły ciąg tekstowy, a nie kod HMTL. Lepiej możemy to zrobić następująco:

```
#!php

<pre>
<?php
echo 'Hello World!';
echo '\r\n';
echo "\r\n";
$counter = 1;
echo 'Counter: $counter';
echo "\r\n";
echo "Counter: $counter";
echo "\r\n";
echo 'Counter: ' . $counter . "\r\n";
?>
</pre>

```

Wszystko co znajduje się poza kontekstami PHP `<?php ?>` jest od razu wysyłane na wyjście, zaś zawartość wewnątrz kontekstu PHP przetwarzana, a wynik przetwarzania wysyłany na wyjście.

Dodajmy jeszcze trochę kodu:

```
#!php

<pre>
<?php
echo 'Hello World!';
echo '\r\n';
echo "\r\n";
$counter = 1;
echo 'Counter: $counter';
echo "\r\n";
echo "Counter: $counter";
echo "\r\n";
echo 'Counter: ' . $counter . "\r\n";
?>
</pre>
<p>
    <?php echo date('d.m.Y'); ?>
</p>

```

W ten sposób w dowolnym miejscu kodu możemy przełączać się pomiędzy kontekstami. Taki kod nosi nazwę _Spaghetti code_. W dalszej części będziemy go używać tylko w szablonach czyli w tej części aplikacji, która będzie odpowiedzialna za wyświetlanie wyników działania skryptów.

## Błędy

Uruchiommy w przeglądarce następujący skrypt:

```
#!php

<pre>
<?php
echo 'Hello World!';
echo '\r\n';
echo "\r\n";
$counter = 1;
echo 'Counter: $counter';
echo "\r\n";
echo "Counter: $counter";
echo "\r\n";
echo 'Counter: ' . $counter . "\r\n";
?>
</pre>
<p>
    <?php echo date('d.m.Y'); ?>
</p>
<?php echo $test; ?>

```

Zmienna `$test` nie została nigdzie zadeklarowana, więc taka linijka kodu powinna zwrócić błąd. To czy bład zostanie zwrócony jest czymś innym niż wyświetlanie błędów. Za oba zachowania odpowiadają różne zmienne w pliku konfiguracyjnym PHP `php.ini`. Za poziom raportowania błędów odpowiada zmienna `error_reporting`, a za to czy są wyświetlane na ekranie `display_errors`. W środowisku produkcyjnym pożądane jest raportowanie wszystkich błędów (do logu serwera), ale już niekoniecznie wyświetlanie ich na ekranie, natomiast w środowisku deweloperskim poza raportowaniem wszystkich błędów, wygodnym jest aby również były wyświetlane na ekranie. Zmodyfikujmy skrypt następująco:

```
#!php

<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
?>
<pre>
<?php
echo 'Hello World!';
echo '\r\n';
echo "\r\n";
$counter = 1;
echo 'Counter: $counter';
echo "\r\n";
echo "Counter: $counter";
echo "\r\n";
echo 'Counter: ' . $counter . "\r\n";
?>
</pre>
<p>
    <?php echo date('d.m.Y'); ?>
</p>
<?php echo $test; ?>

```

## Komentarze w kodzie

W PHP możemy stosować następujące komentarze:

```

#!php

<?php

// komentarz jednolinijkowy

# kolejny komentarz jednolinijkowy

/*
 * komentarz 
 * wielolinijkowy
 */

```













































