# Struktury kontrolne

## if/elseif/else

```
#!php
<?php
$a = 1;
$b = 2;
if ($a > $b) {
    echo '$a większe niż $b';
} elseif ($a == $b) {
    echo '$a równe $b';
} else {
    echo '$a mniejsze niż $b';
}

```

Składnia alternatywna:

```

#!php
<?php
$a = 1;
$b = 2;

if ($a > $b):
    echo '$a większe niż $b';
elseif ($a == $b):
    echo '$a równe $b';
else:
    echo '$a mniejsze niż $b';
endif;


```

## while

```
#!php
<?php
$counter = 0;

while ($counter < 3) {
    echo $counter++ . "\r\n";
}
```

lub

```
#!php
<?php
$counter = 0;

while ($counter < 3):
    echo $counter++ . "\r\n";
endwhile;
```

## do-while

```
#!php
<?php
$counter = 0;

do {
    echo $counter++ . "\r\n";
} while ($counter < 3);

```

## for

```
#!php
<?php
for ($i = 0; $i < 10; $i++) {
    echo $i . "\r\n";
}

```

```
#!php
<?php
$tab = array('apple', 'orange', 'plum');
for ($i = 0, $size = count($tab); $i < $size; $i++) {
    echo $tab[$i] . "\r\n";
}
```

Składnia alternatywna:

```
#!php
<?php
for ($i = 0; $i < 10; $i++):
    echo $i . "\r\n";
endfor;

```

## foreach

```
#!php
<?php
$tab = array('one', 'two', 'three');
foreach ($tab as $value) {
    echo $value . "\r\n";
}

foreach ($tab as $key => $value) {
    echo $key . ' : ' . $value . "\r\n";
}

```

Składnia alternatywna:

```
#!php
<?php
$tab = array('one', 'two', 'three');
foreach ($tab as $value):
    echo $value . "\r\n";
endforeach;

```

```
#!php
<?php
$persons = array(
    array('firstname' => 'Ann', 'surname' => 'Smith'),
    array('firstname' => 'John', 'surname' => 'Doe')
);

foreach ($persons as $row) {
    foreach ($row as $key => $value) {
        echo $key . ' : ' . $value . "\r\n";
    }
}

```

Modyfikacja rekordów:

```
#!php
<?php
$tab = array('one', 'two', 'three');
foreach ($tab as &$value) {
    $value . '_four';
}
unset($value);

var_dump($value);

```
## break

```
#!php
<?php
$tab = array('one', 'two', 'three');
foreach ($tab as $value) {
    if ('two' === $value) {
        break;
    } else {
        echo $value . "\r\n";
    }
}
```

## continue

```
#!php
<?php
$counter = 0;
while ($counter < 10) {
    if ($counter % 2) {
        continue;
    } else {
        echo $counter . "\r\n";
    }
}
```

## switch

```
#!php
$ = 1;
if (0 === $i) {
    echo 'i jest równe 0';
} elseif (1 === $i) {
    echo 'i jest równe 1';
} elseif (2 === $i) {
    echo 'i jest równe 2';
}

switch ($i) {
    case 0:
        echo 'i jest równe 0';
        break;
    case 1:
        echo 'i jest równe 1';
        break;
    case 2:
        echo 'i jest równe 2';
        break;
    default:
        break;
}
```

## require, include, require_once, include_once

Załączenie pliku. `require` działa tak samo jak `include` z tą różnicą, że w przypadku błędu zwraca krytyczny błąd kompilacji, podczas gdy `include` tylko ostrzeżenie co pozwala na nieprzerywanie działania skryptu.
