# Silex VII - mechanizm CRUD - uzupełnienie

## Klasy formularzy

Jak widać, wykorzystanie formularzy w mechanizmie CRUD dla albumów wiąże się tworzeniem ich od nowa dla każdej akcji. Problem ten można rozwiązać poprzez utworzenie klas formularzy. Pracę zaczynamy od zarejestrowania nowej przestrzeni nazw w `composer.json`:

```
#!json
{
    "require": {
        "doctrine/dbal": "2.2.*",
        "silex/silex": "~1.2",
        "symfony/config": "~2.3",
        "symfony/form": "~2.3",
        "symfony/locale": "~2.3",
        "symfony/security-csrf": "~2.3",
        "symfony/translation": "~2.3",
        "symfony/twig-bridge": "~2.3",
        "symfony/validator": "~2.3",
        "symfony/yaml": "~2.3"
    },
    "autoload": {
        "psr-0": {
            "Controller" : "src/",
            "Model" : "src/",
            "Form" : "src/"
        }
    }
}
```

Jak zwykle po takiej operacji musimy wywołać:

```
#!bash
php composer.phar update
```

Kolejnym krokiem jest utworzenie nowego katalogu w katalogu `src`:

```
#!bash
mkdir -p ~/silex_test/src/Form
```

a w nim klasy dla formularza `AlbumForm.php`:

```
#!php
<?php
/**
 * Album form.
 *
 * @author EPI <epi@uj.edu.pl>
 * @link http://epi.uj.edu.pl
 * @copyright 2015 EPI
 */

namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AlbumForm.
 *
 * @category Epi
 * @package Form
 * @extends AbstractType
 * @use Symfony\Component\Form\AbstractType
 * @use Symfony\Component\Form\FormBuilderInterface
 * @use Symfony\Component\OptionsResolver\OptionsResolverInterface
 * @use Symfony\Component\Validator\Constraints as Assert
 */
class AlbumForm extends AbstractType
{
    /**
     * Form builder.
     *
     * @access public
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return  $builder->add(
            'id', 
            'hidden',
            array(
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Type(array('type' => 'digit'))
                )
            )
        )
        ->add(
            'title', 
            'text',
            array(
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Length(array('min' => 5))
                )
            )
        )
        ->add(
            'artist', 
            'text',
            array(
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Length(array('min' => 5))
                )
            )
        );

    }

    /**
     * Gets form name.
     *
     * @access public
     *
     * @return string
     */
    public function getName()
    {
        return 'albumForm';
    }

}
```

Przechodzimy do pliku `AlbumsController.php`, dodajemy nową przestrzeń nazw:

```
#!php
<?php
// ...
use Form\AlbumForm;
// ...
```

a następnie modfyikujemy utworzenie formularza w metodzie `addAction()` następująco:

```
#!php
<?php
// ...
    $form = $app['form.factory']
        ->createBuilder(new AlbumForm(), $data)->getForm();
    $form->remove('id');
// ...
```

Dla metody `editAction()` ta modyfikacja powinna wyglądać następująco:

```
#!php
<?php
// ...
    $form = $app['form.factory']
        ->createBuilder(new AlbumForm(), $album)->getForm();
// ...
```

a dla metody `deleteAction()`:

```
#!php
<?php
// ...
    $form = $app['form.factory']
        ->createBuilder(new AlbumForm(), $album)->getForm();
    $form->remove('title');
    $form->remove('artist');
// ...
```

Pamiętajmy aby z kontrolera `AlbumsController` usunąć zbędną już linię:

```
#!php
<?php
// ...
use Symfony\Component\Validator\Constraints as Assert;
// ...
```

## Ukrycie implementacji dla porcjowania wyników na stronie.

Aby w pełni ukryć implementację porcjowania wyników na stronie, poprzez uproszczenie w kontrolerze kodu:

```
#!php
<?php
// ...
    /**
     * Index action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function indexAction(Application $app, Request $request)
    {
        $pageLimit = 3;
        $page = (int) $request->get('page', 1);
        $albumsModel = new AlbumsModel($app);
        $pagesCount = $albumsModel->countAlbumsPages($pageLimit);
        $page = $albumsModel->getCurrentPageNumber($page, $pagesCount);
        $albums = $albumsModel->getAlbumsPage($page, $pageLimit);
        $this->view['paginator']
            = array('page' => $page, 'pagesCount' => $pagesCount);
        $this->view['albums'] = $albums;
        return $app['twig']->render('albums/index.twig', $this->view);
    }
// ...
```

należy w modelu `AlbumsModel` dodać metodę:

```
#!php
<?php
// ...
    /**
     * Gets albums for pagination.
     *
     * @access public
     * @param integer $page Page number
     * @param integer $limit Number of records on single page
     *
     * @return array Result
     */
    public function getPaginatedAlbums($page, $limit)
    {
        $pagesCount = $this->countAlbumsPages($limit);
        $page = $this->getCurrentPageNumber($page, $pagesCount);
        $albums = $this->getAlbumsPage($page, $limit);
        return array(
            'albums' => $albums,
            'paginator' => array('page' => $page, 'pagesCount' => $pagesCount)
        );
    }
// ...
```

a w kontrolerze `AlbumsController`, metodę `indexAction()` zmienić następująco:

```
#!php
<?php
// ...
    /**
     * Index action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function indexAction(Application $app, Request $request)
    {
        $pageLimit = 3;
        $page = (int) $request->get('page', 1);
        $albumsModel = new AlbumsModel($app);
        $this->view = array_merge(
            $this->view, $albumsModel->getPaginatedAlbums($page, $pageLimit)
        );
        return $app['twig']->render('albums/index.twig', $this->view);
    }
// ...
```

## Ćwiczenie I

Przygotuj mechanizm CRUD dla kategorii. Poniżej znajduje się zrzut struktury dla bazy danych:

```
#!sql
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(128) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
```

Dla ustalenia uwagi, kontroler nazwiemy `CategoriesController` i zamontujemy go w ścieżce: `/categories`.

## Ćwiczenie II

Celem kolejnego ćwiczenia jest powiązanie kategorii z albumami. Rozpocznij od załadowania poniższej aktualizacji struktury do bazy danych:

```
#!sql
ALTER TABLE  `albums` ADD  `category_id` INT UNSIGNED NULL AFTER  `id` ;
ALTER TABLE  `albums` ADD CONSTRAINT  `FK_albums_1` FOREIGN KEY (  `category_id` ) REFERENCES  `categories` (  `id` );
```

Kolejne kroki jakie należy wykonać:

* Aktualizacja mechanizmów pobierania rekordów (lista, pojedynczy rekord), tak aby pobierana była kategoria,
* W klasie `CategoriesModel` przygotuj metodę pozwalającą na pobranie wszystkich kategorii. Wykorzystasz ją do dynamicznego zbudowania zawartości listy rozwijanej w formularzu `AlbumsForm`.
* W klasie `AlbumsForm` stwórz metodę pomocniczą (tryb prywatny lub chroniony), która zwróci listę wszystkich kategorii za pomocą modelu `CategoriesModel`.
* W formularzu `AlbumsForm` dodaj pole typu [`choice`](http://symfony.com/doc/current/reference/forms/types/choice.html "choice"), do którego dynamicznie załaduj listę kategorii za pomocą przygotowanej metody.
* Sprawdź czy nadal poprawnie działa metoda zapisująca album, jeżeli nie zmodufikuj ją.

