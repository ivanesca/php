# Zmienne

Zmienne w PHP poprzedzone są znakiem dolara, po którym następuje nazwa zmiennej. W nazwie zmiennej rozróżnane są małe i duże litery. Wyrażenie regularne opisujące nazwę zmiennej wygląda tak `'[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*'`

Przykłady:

```

#!php

<?php
$foo = 'bar';
var_dump($foo);
$Foo = 123;
var_dump($Foo);
$2string = 'some string';
var_dump($2string);
$_2string = 'some string';
var_dump($_2string);

```

## Zmienne predefiniowane

* `$GLOBALS` - wszystkie zmienne dostępne w zasięgu globalnym,

```
  
#!php
  
<?php
$a = 123;
var_dump($a);
var_dump($GLOBALS['a']);
  
```

* `$_SERVER` - tablica zmiennych środowiskowych serwera,
* `$_GET` - tablica zmiennych przesyłanych metodą GET,
* `$_POST` - tablica zmienych przesyłanych metodą POST,
* `$_FILES` - tablica przechowująca przesyłane pliki,
* `$_REQUEST` - tablica zmiennych POST I GET,
* `$_SESSION` - tablica zmiennych sesyjnych,
* `$_ENV` - tablica zmiennych środowiskowych,
* `$_COOKIE` - tablica zmiennych pochodzących z ciasteczek,
* `$php_errormsg` - poprzednia wiadomość o błędzie,
* `$HTTP_RAW_POST_DATA` - tablica danych Raw POST,
* `$http_response_header` — nagłówki odpowiedzi HTTP,
* `$argc` — liczba argumentów przesłanych do skryptu,
* `$argv` — tablica argumentów przesłanych do skryptu.

###  Odbieranie danych GET

Na serwerze umieszczamy skrypt `test_get.php`:

```

#!php
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
echo $_GET['foo'];

```

Wywołajmy go w przeglądarce poprzez adres: `http://wierzba.wzks.uj.edu.pl/~[USER]/test_get.php`.
Zmieńmy wywołanie na `http://wierzba.wzks.uj.edu.pl/~[USER]/test_get.php?foo=bar`.

Skrypt poprawmy następująco:

```

#!php
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
echo isset($_GET['foo'])?$_GET['foo']:'';

```
Teraz ponownie możemy wywołać pierwszy z adresów.

### Ćwiczenie 1.

Proszę przygotować formularz zawierający jedną zmienną `foo` przesyłany metodą `POST` oraz skrypt, który ją odbierze i wyświetli w przeglądarce.

### Ćwiczenie 2.

Jaka  jest różnica pomiędzy funkcjami `isset()` i `empty()`?

## Zasięg zmiennych

```

#!php
<?php
$a = 1;
$b = 2;

var_dump($a + $b);

function foo()
{
    $a = -1;
    $b = 0;
    
    var_dump($a + $b);
    var_dump($GLOBALS['a'] + $GLOBALS['b']);
}

foo();

```

## Zmienne zmiennych


```

#!php
<?php
$var = 'Hello';
$$var = 'World!';

echo $var . ' ' . ${$var};
echo "\r\n";
echo $var . ' ' . $Hello;
echo "\r\n";

```
