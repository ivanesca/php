# Silex VI - wprowadzenie do obsługi bazy danych

## Przygotowanie

Zaczynamy od przygotowania struktury bazy danych. Dla uproszczenia zaczniemy od takiej struktury, którą umieścimy w skrypcie `albums.sql` w katalogu `silex_test/data`:

```
#!sql
CREATE TABLE IF NOT EXISTS `albums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(64) COLLATE utf8_bin NOT NULL,
  `artist` char(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `albums` (`id`, `title`, `artist`) VALUES
(1, 'Michael Jackson', 'Thriller'),
(2, 'Pink Floyd', 'The Dark Side of the Moon'),
(3, 'Whitney Houston / Various artists', 'The Bodyguard'),
(4, 'Meat Loaf', 'Bat Out of Hell'),
(5, 'Eagles', 'Their Greatest Hits (1971–1975)'),
(6, 'AC/DC', 'Back in Black'),
(7, 'Bee Gees / Various artists', 'Saturday Night Fever'),
(8, 'Fleetwood Mac', 'Rumours');
```
Strukturę wyczytujemy do bazy danych:

```
#!bash
mysql -u USER -p
use DB_NAME;
source /path/to/albums.sql
```

## Konfiguracja aplikacji

Plik `composer.json` uzupełniam o wymagane biblioteki i przy okazji rejestruję nową przestrzeń nazw:

```
#!json
{
    "require": {
        "doctrine/dbal": "2.2.*",
        "silex/silex": "~1.2",
        "symfony/config": "~2.3",
        "symfony/translation": "~2.3",
        "symfony/twig-bridge": "~2.3",
        "symfony/yaml": "~2.3"
    },
    "autoload": {
        "psr-0": {
            "Controller" : "src/",
            "Model" : "src/"
        }
    }

}
```

i aktualizujemy `composer'a`:

```
#!bash
php composer.phar update
```

W pliku `index.php` rejestruję nową usługę:

```
#!php
<?php
// ...
$app->register(
    new Silex\Provider\DoctrineServiceProvider(), 
    array(
        'db.options' => array(
            'driver'    => 'pdo_mysql',
            'host'      => 'localhost',
            'dbname'    => '',
            'user'      => '',
            'password'  => '',
            'charset'   => 'utf8',
        ),
    )
);
// ...
```

## Przykładowa aplikacja

Celem jest przygotowanie mechanimów w pobierania wszystkich rekordów i wyświelania pojedynczego rekordu z bazy danych. Podobnie jak podczas poprzednich laboratoriów przygotujemy kontroler `AlbumsController` a w nim dwie akcje `indexAction()` i `viewAction()`. Adresy URL jakie będziemy wywoływać będą wyglądać następująco:

```
#!bash
http://example.com/albums
http://example.com/albums/view/XYZ
```

Pracę zaczynamy od przygotowania kontrolera `AlbumsController`:

```
#!php
<?php
/**
 * Albums controller.
 *
 * @author EPI <epi@uj.edu.pl>
 * @link http://epi.uj.edu.pl
 * @copyright 2015 EPI
 */

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AlbumsController.
 *
 * @category Epi
 * @package Controller
 * @implements ControllerProviderInterface
 *
 */
class AlbumsController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @access public
     * @param Silex\Application $app Silex application
     */
    public function connect(Application $app)
    {
        $albumsController = $app['controllers_factory'];
        $albumsController->get('/', array($this, 'indexAction'))->bind('albums_index');
        $albumsController->get('/index', array($this, 'indexAction'));
        $albumsController->get('/index/', array($this, 'indexAction'));
        $albumsController->get('/view/{id}', array($this, 'viewAction'))->bind('albums_view');
        $albumsController->get('/view/{id}/', array($this, 'viewAction'));
        return $albumsController;
    }

    /**
     * Index action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function indexAction(Application $app, Request $request)
    {
        $view = array();
        return $app['twig']->render('albums/index.twig', $view);
    }

    /**
     * View action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function viewAction(Application $app, Request $request)
    {
        $view = array();
        $id = (int)$request->get('id', null);
        return $app['twig']->render('albums/view.twig', $view);
    }

}
```

Tworzymy katalog `albums` w katalogu `src/views`, a w nim dwa szablony `index.twig` i `view.twig`:

```
#!twig
{% extends 'base.twig' %}

{% block content %}
    <p>
        indexAction of AlbumsController
    </p>
{% endblock %}
```

```
#!twig
{% extends 'base.twig' %}

{% block content %}
    <p>
        viewAction of AlbumsController
    </p>
{% endblock %}
```

Pozostaje zarejestować nowy kontroler w pliku `index.php`:

```
#!php
<?php
// ...
$app->mount('/albums', new Controller\AlbumsController());
// ...
```

Po tym przygotowaniu w katalogu `src` tworzymy katalog na modele:

```
#!bash
cd ~/silex_test
mkdir -p src/Model
```

Przechodzimy do przygotwania klasy modelu dla albumów. Zatem w katalogu `src/Model` tworzymy plik `AlbumsModel.php` o następującej zawartości:

```
#!php
<?php
/**
 * Albums model.
 *
 * @author EPI <epi@uj.edu.pl>
 * @link http://epi.uj.edu.pl
 * @copyright 2015 EPI
 */

namespace Model;

use Silex\Application;

/**
 * Class AlbumsModel.
 *
 * @category Epi
 * @package Model
 * @use Silex\Application
 */
class AlbumsModel
{
    /**
     * Db object.
     *
     * @access protected
     * @var Silex\Provider\DoctrineServiceProvider $db
     */
    protected $db;

    /**
     * Object constructor.
     *
     * @access public
     * @param Silex\Application $app Silex application
     */
    public function __construct(Application $app)
    {
        $this->db = $app['db'];
    }

    /**
     * Gets all albums.
     *
     * @access public
     * @return array Result
     */
    public function getAll()
    {
        $query = 'SELECT id, title, artist FROM albums';
        $result = $this->db->fetchAll($query);
        return !$result ? array() : $result;
    }

    /**
     * Gets single album data.
     *
     * @access public
     * @param integer $id Record Id
     * @return array Result
     */
    public function getAlbum($id)
    {
        if (($id != '') && ctype_digit((string)$id)) {
            $query = 'SELECT id, title, artist FROM albums WHERE id= :id';
            $statement = $this->db->prepare($query);
            $statement->bindValue('id', $id, \PDO::PARAM_INT);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return !$result ? array() : current($result);
        } else {
            return array();
        }
    }
}
```

Ponieważ kontroler `AlbumsController` ma korzystać z modelu powyżej na do listy deklaracji `use` należy dopisać:

```
#!php
<?php
// ...
use Model\AlbumsModel;
```

i wywołać metody modelu w poszczególnych akcjach. Cały kontroler będzie wyglądać tak:

```
#!php
<?php
/**
 * Albums controller.
 *
 * @author EPI <epi@uj.edu.pl>
 * @link http://epi.uj.edu.pl
 * @copyright 2015 EPI
 */

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Model\AlbumsModel;

/**
 * Class AlbumsController.
 *
 * @category Epi
 * @package Controller
 * @implements ControllerProviderInterface
 *
 */
class AlbumsController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @access public
     * @param Silex\Application $app Silex application
     */
    public function connect(Application $app)
    {
        $albumsController = $app['controllers_factory'];
        $albumsController->get('/', array($this, 'indexAction'))->bind('albums_index');
        $albumsController->get('/index', array($this, 'indexAction'));
        $albumsController->get('/index/', array($this, 'indexAction'));
        $albumsController->get('/view/{id}', array($this, 'viewAction'))->bind('albums_view');
        $albumsController->get('/view/{id}/', array($this, 'viewAction'));
        return $albumsController;
    }

    /**
     * Index action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function indexAction(Application $app, Request $request)
    {
        $view = array();
        $albumsModel = new AlbumsModel($app);
        $view['albums'] = $albumsModel->getAll();
        return $app['twig']->render('albums/index.twig', $view);
    }

    /**
     * View action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function viewAction(Application $app, Request $request)
    {
        $view = array();
        $id = (int)$request->get('id', null);
        $albumsModel = new AlbumsModel($app);
        $view['album'] = $albumsModel->getAlbum($id);
        return $app['twig']->render('albums/view.twig', $view);
    }

}
```

Jako proste ćwiczenie zostawiam uzupełnienie plików widoków.

## Ćwiczenie I

Każda metoda modelu powinna zrzucać wyjątek. Uzupełnij metody modelu i kontroler o obsługę wyjątków. Jakiego typu wyjątek zrzuca `Silex\Provider\DoctrineServiceProvider`?

## Ćwiczenie II

Przygotuj mechanizm pozwalający na wyświetlenie na jednej stronie określonej liczby rekordów (np. 3) z możliwością przechodzenia pomiędzy poszczególnymi stronami. URL jakie będziemy chcieli wywoływać wyglądają następująco:

```
#!bash
http://example.com/pages - wyświetlenie pierwszej strony, rekody od 1 do 3.
http://example.com/pages/1 - wyświetlenie pierwszej strony, rekody od 1 do 3.
http://example.com/pages/2 - wyświetlenie drugiej strony, rekody od 4 do 6.
// etc.
```

## Ćwiczenie III

W klasie modelu przygotuj metody odpowiedzialne za dodanie, edycję i usunięcie pojedynczego elementu.

