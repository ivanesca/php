# Klasy i obiekty

## Klasa - pola/właściwości

```
#!php
<?php
/**
 *
 */
class Person
{
    public $firstname;
    public $surname;
}
```

```
#!php
<?php
require dirname(__FILE__) . '/dev.php';
require dirname(__FILE__) . '/Person.php';

$person = new Person();

$person->firstname = 'John';
$person->surname = 'Doe';

echo $person->firstname . ' ' . $person->surname;
```

## Klasa - tryb dostępu

```
#!php
<?php
/**
 *
 */
class Person
{
    private $firstname;
    private $surname;
}
```

```
#!php
<?php
require dirname(__FILE__) . '/dev.php';
require dirname(__FILE__) . '/Person.php';

$person = new Person();

$person->firstname = 'John';
$person->surname = 'Doe';

echo $person->firstname . ' ' . $person->surname;
```

```
#!php
<?php
/**
 *
 */
class Person
{
    private $_firstname;
    private $_surname;
    
    public function setFirstname($firstname)
    {
        $this->_firstname = (string)$firstname;
    }
    
    public function getFirstname()
    {
        return $this->_firstname;
    }
    
    public function setSurname($surname)
    {
        $this->_surname = (string)$surname;
    }
    
    public function getSurname()
    {
        return $this->_surname;
    }    
    
}
```

```
#!php
<?php
require dirname(__FILE__) . '/dev.php';
require dirname(__FILE__) . '/Person.php';

$person = new Person();

$person->setFirstname('John');
$person->setSurname('Doe');

echo $person->getFirstname() . ' ' . $person->getSurname();
```

## Klasa - konstruktor, destruktor

```
#!php
<?php
/**
 * Person class.
 *
 * @link http://epi.uj.edu.pl
 * @author epi(at)uj(dot)edu(dot)pl
 * @copyright EPI 2015
 */
class Person
{
    /**
     * Firstname.
     *
     * @access private
     * @var string $_firstname
     */
    private $_firstname;

    /**
     * Surname.
     *
     * @access private
     * @var string $_surname
     */
    private $_surname;

    /**
     * Class constructor.
     *
     * @access public
     * @param string $firstname Firstname
     * @param string $surname Surname
     */
    public function __construct($firstname, $surname)
    {
        $this->_setFirstname($firstname);
        $this->_setSurname($surname);
    }

    /**
     * Setter for firstname.
     *
     * @access private
     * @param string $firstname Firstname
     */
    private function _setFirstname($firstname)
    {
        $this->_firstname = (string)$firstname;
    }

    /**
     * Getter for firstname.
     *
     * @access public
     * @return string Result
     */
    public function getFirstname()
    {
        return $this->_firstname;
    }

    /**
     * Setter for surname.
     *
     * @access private
     * @param string $surname Surname
     */
    private function _setSurname($surname)
    {
        $this->_surname = (string)$surname;
    }

    /**
     * Getter for surname.
     *
     * @access public
     * @return string Result
     */
    public function getSurname()
    {
        return $this->_surname;
    }

}
```

```
#!php
<?php

class Db
{

// ....

    /**
     * Class destructor.
     *
     * @access public
     * @return mixed Result
     */
    public function __destruct()
    {
        return mysql_close();
    }


}
```

## Dziedziczenie

```
#!php
<?php
class Animal
{
    protected $_name;
    
    public function __construct($name)
    {
        $this->_name = (string)$name;
    }
    
    public function getName()
    {
        return $this->_name;
    }
    
}
```

```
#!php
<?php
class Dog extends Animal
{
    public function __construct($name)
    {
        parent::__construct($name);
    }

    public function bark()
    {
        echo 'Woof woof!';
    }
}
```

```
#!php
<?php
class Cat extends Animal
{
    public function __construct($name)
    {
        parent::__construct($name);
    }
    
    public function meow()
    {
        echo 'Meow...';
    }
}
```

```
#!php
<?php
require dirname(__FILE__) . '/dev.php';
require dirname(__FILE__) . '/Animal.php';
require dirname(__FILE__) . '/Dog.php';
require dirname(__FILE__) . '/Cat.php';

$cat = new Cat('Carmel');
echo $cat->meow();
echo $cat->getName();
$dog = new Dog('Admiral');
echo $dog->bark();
echo $dog->meow();
```
## Autoładowanie klas

```
#!php
<?php

require dirname(__FILE__) . '/dev.php';

function __autoload($class_name) {
    require dirname(__FILE__) . '/' . $class_name . '.php';
}

$cat = new Cat('Carmel');
echo $cat->meow();
echo $cat->getName();
$dog = new Dog('Admiral');
echo $dog->bark();
```

## Pola i metody statyczne

```
#!php
<?php

class Math
{
    static public function inverse($x)
    {
        if (!$x) {
            return $x;
        }
        return 1/$x;
    }
}
```

```
#!php
<?php

require dirname(__FILE__) . '/dev.php';
require dirname(__FILE__) . '/Math.php';

echo Math::inverse(2);
```

## Interfejsy

```
<?php

interface iTemplateInterface
{
    public function setVariable($name, $var);
    public function getHtml($template);
}
```

```
#!php
<?php
class Template implements iTemplateInterface
{
    private $vars = array();
  
    public function setVariable($name, $var)
    {
        $this->vars[$name] = $var;
    }
  
    public function getHtml($template)
    {
        foreach($this->vars as $name => $value) {
            $template = str_replace('{' . $name . '}', $value, $template);
        }
 
        return $template;
    }
}
```

## Wyjątki

```
#!php
<?php
class MathException extends Exception
{
}

```

```
#!php
<?php
require_once dirname(__FILE__) . '/MathException.php';
class Math
{
    static public function inverse($x)
    {
        if (!$x) {
            throw new MathException('Division by zero!');
        }
        return 1/$x;
    }
}
```

```
#!php
<?php

require_once dirname(__FILE__) . '/dev.php';
require_once dirname(__FILE__) . '/MathException.php';
require_once dirname(__FILE__) . '/Math.php';

try {
    try {
        var_dump(Math::inverse(5));
        var_dump(Math::inverse(0));
    } catch (MathException $e) {
        echo 'MathException!';   
    }
} catch (Exception $e) {
    echo 'Caught exception: ' .  $e->getMessage() . "\n";
}

var_dump('Hello World!');
```
