# Funkcje 

```
#!php
<?php
/**
 * Recursion demo.
 *
 * @param integer $var Number
 */
function recursion($var) {
    if ($var < 20) {
        echo $var . "\n";
        recursion($var + 1);
    }
}

```

