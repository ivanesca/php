# Silex VII - mechanizm CRUD - przykład

## Porcjowanie wyników na stronie

W modelu `AlbumsModel.php` dodajemy następujące metody:
 
```
#!php
<?php
// ...
    /**
     * Get all albums on page.
     *
     * @access public
     * @param integer $page Page number
     * @param integer $limit Number of records on single page
     * @retun array Result
     */
    public function getAlbumsPage($page, $limit)
    {
        $query = 'SELECT id, title, artist FROM albums LIMIT :start, :limit';
        $statement = $this->db->prepare($query);
        $statement->bindValue('start', ($page-1)*$limit, \PDO::PARAM_INT);
        $statement->bindValue('limit', $limit, \PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return !$result ? array() : $result;
    }

    /**
     * Counts album pages.
     *
     * @access public
     * @param integer $limit Number of records on single page
     * @return integer Result
     */
    public function countAlbumsPages($limit)
    {
        $pagesCount = 0;
        $sql = 'SELECT COUNT(*) as pages_count FROM albums';
        $result = $this->db->fetchAssoc($sql);
        if ($result) {
            $pagesCount =  ceil($result['pages_count']/$limit);
        }
        return $pagesCount;
    }

    /**
     * Returns current page number.
     *
     * @access public
     * @param integer $page Page number
     * @param integer $pagesCount Number of all pages
     * @return integer Page number
     */
    public function getCurrentPageNumber($page, $pagesCount)
    {
        return (($page < 1) || ($page > $pagesCount)) ? 1 : $page;
    }
// ...
```

Widok dla listy elementów `albums/index.twig` przyjmie postać:

```
#!twig
{# Index action template for Albums controller #}
{% extends 'base.twig' %}

{% block title %}{{  'Albums list'|trans }}{% endblock %}

{% block content %}
    <h1>
        {{ 'Albums list'|trans }}
    </h1>
    <p>
        <a href="{{ url('albums_add') }}" title="{{ 'Add album'|trans }}">
            {{ 'Add album'|trans }}
        </a>
    </p>

    {%  if albums|length > 0 %}

        <div>
            {% if paginator.page > 1 %}
                {% set previous = (paginator.page - 1) %}
                <a href="{{ url('albums_index', {'page': previous}) }}" title="{{ 'previous page'|trans }}">{{ 'previous page'|trans }}</a>
            {%  endif %}

            {% if paginator.page < paginator.pagesCount %}
                {% set next = (paginator.page + 1) %}
                <a href="{{ url('albums_index', {'page': next}) }}" title="{{ 'next page'|trans }}">{{ 'next page'|trans }}</a>
            {% endif  %}
        </div>

        <table>
            <thead>
            <tr>
                <th>{{ 'Id'|trans }}</th>
                <th>{{ 'Title'|trans }}</th>
                <th>{{ 'Artist'|trans }}</th>
                <th colspan="3">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            {% for album in albums %}
                <tr>
                    <td>{{ album.id|e }}</td>
                    <td>{{ album.title|e }}</td>
                    <td>{{ album.artist|e }}</td>
                    <td>
                        <a href="{{ url('albums_view', {'id': album.id}) }}" title="{{ 'View album'|trans }}">{{ 'View album'|trans }}</a>
                    </td>
                    <td>
                        <a href="{{ url('albums_edit', {'id': album.id}) }}" title="{{ 'Edit album'|trans }}">{{ 'Edit album'|trans }}</a>
                    </td>
                    <td>
                        <a href="{{ url('albums_delete', {'id': album.id}) }}" title="{{ 'Delete album'|trans }}">{{ 'Delete album'|trans }}</a>
                    </td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
    {% else %}
        <div class="alert alert-danger" role="alert">
            {{ 'Albums not found'|trans }}.
        </div>
    {% endif %}
{% endblock %}
```

Kontroler `AlbumsController` będzie wyglądać następująco:

```
#!php
<?php
/**
 * Albums controller.
 *
 * @author EPI <epi@uj.edu.pl>
 * @link http://epi.uj.edu.pl
 * @copyright 2015 EPI
 */

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Model\AlbumsModel;

/**
 * Class AlbumsController.
 *
 * @category Epi
 * @package Controller
 * @implements ControllerProviderInterface
 *
 */
class AlbumsController implements ControllerProviderInterface
{

    /**
     * Data for view.
     *
     * @access protected
     * @var array $view
     */
    protected $view = array();

    /**
     * Routing settings.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @return AlbumsController Result
     */
    public function connect(Application $app)
    {
        $albumsController = $app['controllers_factory'];
        $albumsController->post('/add', array($this, 'addAction'));
        $albumsController->match('/add', array($this, 'addAction'))
            ->bind('albums_add');
        $albumsController->match('/add/', array($this, 'addAction'));
        $albumsController->post('/edit/{id}', array($this, 'editAction'));
        $albumsController->match('/edit/{id}', array($this, 'editAction'))
            ->bind('albums_edit');
        $albumsController->match('/edit/{id}/', array($this, 'editAction'));
        $albumsController->post('/delete/{id}', array($this, 'deleteAction'));
        $albumsController->match('/delete/{id}', array($this, 'deleteAction'))
            ->bind('albums_delete');
        $albumsController->match('/delete/{id}/', array($this, 'deleteAction'));
        $albumsController->get('/view/{id}', array($this, 'viewAction'))
            ->bind('albums_view');
        $albumsController->get('/view/{id}/', array($this, 'viewAction'));
        $albumsController->get('/index', array($this, 'indexAction'));
        $albumsController->get('/index/', array($this, 'indexAction'));
        $albumsController->get('/{page}', array($this, 'indexAction'))
                         ->value('page', 1)->bind('albums_index');
        return $albumsController;
    }

    /**
     * Index action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function indexAction(Application $app, Request $request)
    {
        $pageLimit = 3;
        $page = (int) $request->get('page', 1);
        $albumsModel = new AlbumsModel($app);
        $pagesCount = $albumsModel->countAlbumsPages($pageLimit);
        $page = $albumsModel->getCurrentPageNumber($page, $pagesCount);
        $albums = $albumsModel->getAlbumsPage($page, $pageLimit);
        $this->view['paginator']
            = array('page' => $page, 'pagesCount' => $pagesCount);
        $this->view['albums'] = $albums;
        return $app['twig']->render('albums/index.twig', $this->view);
    }

    /**
     * View action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function viewAction(Application $app, Request $request)
    {
        $id = (int)$request->get('id', null);
        $albumsModel = new AlbumsModel($app);
        $this->view['album'] = $albumsModel->getAlbum($id);
        return $app['twig']->render('albums/view.twig', $this->view);
    }

}
```

Przy okazji zmian, przenieśliśmy tablicę przechowującą dane dla szablonu do pól obiektu oraz dodaliśmy brakujące ścieżku routingu dla mechanizmu CRUD.
  
## Dodawanie nowego rekordu

Zaczynamy od załadowania komponentów do obsługi formularzy i walidacji. Otwieramy `composer.json` i aktualizujemy zgodnie z poniższym listingiem:

```
#!json
{
    "require": {
        "doctrine/dbal": "2.2.*",
        "silex/silex": "~1.2",
        "symfony/config": "~2.3",
        "symfony/form": "~2.3",
        "symfony/locale": "~2.3",
        "symfony/security-csrf": "~2.3",
        "symfony/translation": "~2.3",
        "symfony/twig-bridge": "~2.3",
        "symfony/validator": "~2.3",
        "symfony/yaml": "~2.3"
    },
    "autoload": {
        "psr-0": {
            "Controller" : "src/",
            "Model" : "src/"
        }
    }
}
```

Jak zwykle po takiej operacji musimy wywołać:

```
#!bash
php composer.phar update
```

Następnie w pliku `index.php` rejestrujemy usługi:

```
#!php
<?php
// ...
$app->register(new Silex\Provider\FormServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
// ...
```

Przygotujmy pierwszą wersję akcji `addAction()`:

```
#!php
<?php
// ...
    /**
     * Add action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function addAction(Application $app, Request $request)
    {
        // default values:
        $data = array(
            'title' => 'Title',
            'artist' => 'Artist',
        );

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('title')->add('artist')->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            var_dump($data);
        }

        $this->view['form'] = $form->createView();

        return $app['twig']->render('albums/add.twig', $this->view);
    }
// ... 
```

Szablon dla widoku `albums/add.twig` będzie wyglądał tak:

```
#!twig
{# Add action template for Albums controller #}
{% extends 'base.twig' %}

{% block title %}{{  'Add album'|trans }}{% endblock %}

{% block content %}
    <h1>
        {{ 'Add album'|trans }}
    </h1>
    <form action="{{ url('albums_add') }}" method="post">
        {{ form_widget(form) }}
        <input type="submit" name="submit" value="{{ 'Save'|trans }}" />
    </form>
{% endblock %}
```

Dodajmy mechanizmy walidacji do akcji:

```
#!php
<?php
// ...
use Symfony\Component\Validator\Constraints as Assert;
// ...
    /**
     * Add action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function addAction(Application $app, Request $request)
    {
        // default values:
        $data = array(
            'title' => 'Title',
            'artist' => 'Artist',
        );

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add(
                'title', 'text',
                array(
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Length(array('min' => 5))
                    )
                )
            )
            ->add(
                'artist', 'text',
                array(
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Length(array('min' => 5))
                    )
                )
            )
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            var_dump($data);
        }

        $this->view['form'] = $form->createView();

        return $app['twig']->render('albums/add.twig', $this->view);
    }
// ...
```

Dodajmy metodę zapisującą rekord do klasy modelu `AlbumsModel.php`:

```
#!php
<?php
// ...
     * Save album.
     *
     * @access public
     * @param array $album Album data
     * @retun mixed Result
     */
    public function saveAlbum($album)
    {
        if (isset($album['id'])
            && ($album['id'] != '')
            && ctype_digit((string)$album['id'])) {
            // update record
            $id = $album['id'];
            unset($album['id']);
            return $this->db->update('albums', $album, array('id' => $id));
        } else {
            // add new record
            return $this->db->insert('albums', $album);
        }
    }
// ...
```

Akcja przyjmie postać:

```
#!php
<?php
// ...
    /**
     * Add action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function addAction(Application $app, Request $request)
    {
        // default values:
        $data = array(
            'title' => 'Title',
            'artist' => 'Artist',
        );

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add(
                'title', 'text',
                array(
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Length(array('min' => 5))
                    )
                )
            )
            ->add(
                'artist', 'text',
                array(
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Length(array('min' => 5))
                    )
                )
            )
            ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $albumsModel = new AlbumsModel($app);
            $albumsModel->saveAlbum($data);
        }

        $this->view['form'] = $form->createView();

        return $app['twig']->render('albums/add.twig', $this->view);
    }
// ...
```

## Przekierowanie na listę po dodaniu nowego albumu

W metodzie `addAction()` fragment kodu zapisujący album należy zmodufikować następująco:
 
```
#!php
<?php
// ...
        if ($form->isValid()) {
            $data = $form->getData();
            $albumsModel = new AlbumsModel($app);
            $albumsModel->saveAlbum($data);
            return $app->redirect(
                $app['url_generator']->generate('albums_index'), 
                301
            );
        }
// ...
```

## Edycja elementu

Zauważmy, że edycja elementu różni się tym od dodawania nowego, że formularz wyświetlany po raz pierwszy musi zostać uzupełniony danymi albumu pobranymi z bazy danych. Zatem `editAction()` przyjmie postać:

```
#!php
<?php
// ...
    /**
     * Edit action.
     *
     * @access public
     * @param Silex\Application $app Silex application
     * @param Symfony\Component\HttpFoundation\Request $request Request object
     * @return string Output
     */
    public function editAction(Application $app, Request $request)
    {

        $albumsModel = new AlbumsModel($app);
        $id = (int) $request->get('id', 0);
        $album = $albumsModel->getAlbum($id);

        if (count($album)) {

            $form = $app['form.factory']->createBuilder('form', $album)
                ->add(
                    'id', 'hidden',
                    array(
                        'constraints' => array(
                            new Assert\NotBlank(),
                            new Assert\Type(array('type' => 'digit'))
                        )
                    )
                )
                ->add(
                    'title', 'text',
                    array(
                        'constraints' => array(
                            new Assert\NotBlank(),
                            new Assert\Length(array('min' => 5))
                        )
                    )
                )
                ->add(
                    'artist', 'text',
                    array(
                        'constraints' => array(
                            new Assert\NotBlank(),
                            new Assert\Length(array('min' => 5))
                        )
                    )
                )
                ->getForm();
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $albumsModel = new AlbumsModel($app);
                $albumsModel->saveAlbum($data);
                return $app->redirect(
                    $app['url_generator']->generate('albums_index'), 
                    301
                );
            }

            $this->view['id'] = $id;
            $this->view['form'] = $form->createView();

        } else {
            return $app->redirect(
                $app['url_generator']->generate('albums_add'), 
                301
            );
        }

        return $app['twig']->render('albums/edit.twig', $this->view);
    }
// ...
```

## Ćwiczenie I

Uzupełnij widok dla edycji elementu

## Ćwiczenie II

Przygotuj mechanizm usuwania elementu. Usunięcie elementu wymaga potwierdzenia operacji przez użytkownika, zatem po kliknięciu na odnośnik usuwania należy wyświetlić formularz z prośbą o potwierdzenie tej operacji.

## Komunikaty po wykonaniu operacji

Rejestrujemy usługę obsługi sesji w pliku `index.php`:

```
#!php
<?php
// ...
$app->register(new Silex\Provider\SessionServiceProvider());
// ...
```
Dla ustalenia uwagi zajmiemy się operacją dodawania nowego elementu. Po poprawnym dodaniu i przekierowaniu na listę albumów chcemy wyświetlić komunikat o powodzeniu operacji. Do szablonu `albums/index.twig` dodajemy następujący kod:
 
```
#!twig
{% for flashMessage in app.session.getFlashBag.get('message') %}t
    <div class="alert alert-{{ flashMessage.type }}" role="alert">
        {{ flashMessage.content }}
    </div>
{% endfor %}
```

a w akcji `addAction()`, tuż przed przekierowaniem na listę dodajemy:

```
#!php
<?php
// ...
$app['session']->getFlashBag()->add(
    'message', array(
        'type' => 'success', 'content' => $app['translator']->trans('New album added.')
    )
);
// ...
```

## Materiały dodatkowe

* [Doctrine DBAL](http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest " Doctrine DBAL") - nas głównie interesuje [4. Data Retrieval And Manipulation](http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/data-retrieval-and-manipulation.html "4. Data Retrieval And Manipulation"),
* [Formularze](http://symfony.com/doc/current/book/forms.html "Formularze"),
* [Walidatory](http://symfony.com/doc/current/book/validation.html "Walidatory"),




